const argv = require('yargs').argv;

const gulp = require('gulp');
const gulpif = require('gulp-if');
const concat = require('gulp-concat');
const ngAnnotate = require('gulp-ng-annotate');
const templateCache = require('gulp-angular-templatecache');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const csso = require('gulp-csso');
const uglify = require('gulp-uglify');
const plumber = require('gulp-plumber');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();
const browserify = require('browserify');
const babelify = require('babelify');
const buffer = require('vinyl-buffer');
const log = require('gulplog');
const watchify = require('watchify');
const assign = require('lodash.assign');
const source = require('vinyl-source-stream');

const src = {
  sass: ['src/main/webapp/app/public/styles/*.scss'],
  react:  ['src/main/webapp/app/root/root.jsx']
};

const dest = {
  sass: {
    dir: 'src/main/webapp/public/build/css',
    file: 'app.css'
  },
  react: {
    dir: 'src/main/webapp/public/build/react',
    file: 'app.js'
  }
};

gulp.task('sass', () => {
  return gulp.src(src.sass)
  .pipe(plumber())
  .pipe(sass())
  .pipe(concat(dest.sass.file))
  .pipe(autoprefixer())
  // remove csso because breaks on rangeerror (using datauri for slider images to remove "flashing" since grodw doesnt like that)
  // .pipe(gulpif(argv.production, csso()))
  .pipe(gulp.dest(dest.sass.dir))
  .pipe(browserSync.stream());
});


// add custom browserify options here
const customOpts = {
  entries: src.react, //Entry point at root.jsx, imports will be recognized and added as they appears
  debug: true
};
const opts = assign({}, watchify.args, customOpts); // watchify.args includes the arguments like 'cache' that watchify needs to use to run
const watch = watchify(browserify(opts).transform('browserify-css', {global: true}));

// transformations here
watch.transform(babelify.configure({ presets: ["@babel/preset-env", "@babel/preset-react"] }));


gulp.task('react', argv.production ? () => {
  let browserifyStream = browserify(opts).transform('browserify-css', {global: true});
  browserifyStream.transform(babelify.configure({ presets: ["@babel/preset-env", "@babel/preset-react"] }));

  return browserifyStream.bundle()
  .on('error', log.error.bind(log, 'Browserify Error'))  // log errors if they happen
  .pipe(source(dest.react.file)) //compile the jsx into a single file
  .pipe(buffer())
  .pipe(sourcemaps.init({loadMaps: true}))
  .pipe(concat(dest.react.file))
  // .pipe(gulpif(argv.production, uglify())).on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
  .pipe(sourcemaps.write('./')) //create mappings at piped dir
  .pipe(gulp.dest(dest.react.dir));

  // return gulp.src(src.react)
  // .pipe(buffer())
  // .pipe(sourcemaps.init({loadMaps: true}))
  // // .pipe(gulpif(argv.production, uglify())).on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
  // .pipe(sourcemaps.write('./')) //create mappings at piped dir
  // .pipe(gulp.dest(dest.react.dir));
} : compileReact);
watch.on('update', compileReact); // on any dep update, runs the bundler
watch.on('log', log.info); // output build logs to terminal.

// Note, there's a delay when watchify compiles the jsx and when it actually finish writing the build to file. Causing the browser to
// to use the old version if you refresh the page too fast.
// There are ways to tell the browser to wait for the new build to finish, but they won't work with our architecture as they
// require the server to be open here to delay the http request
function compileReact() {
  log.info('Compiling jsx files');
  log.info('There is a delay of around 1 second after the bytes is written where the browser will continue to use the old react code');
  log.info('Check the comments on gulpfile.js');
  return watch.bundle()
    .on('error', log.error.bind(log, 'Browserify Error'))  // log errors if they happen
    .pipe(source(dest.react.file)) //compile the jsx into a single file
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(gulpif(argv.production, uglify()))
    .pipe(sourcemaps.write('./')) //create mappings at piped dir
    .pipe(gulp.dest(dest.react.dir));
}

gulp.task('apply-prod-environment', function() {
  process.env.NODE_ENV = 'production';
});

gulp.task('watch', () => {
  gulp.watch(src.sass, ['sass']);
  gulp.watch(src.react, ['react']);
});

// create a task that ensures the `js` task is complete before
// reloading browsers
gulp.task('js-watch', ['react'], function (done) {
  browserSync.reload();
  done();
});
gulp.task('buildProduction', ['apply-prod-environment', 'build']);
gulp.task('build', ['sass', 'react']);
gulp.task('default', ['build', 'watch'], () => {
      // Serve files from the root of this project
      browserSync.init({
        port: "10011",
        proxy: {
          target: "https://localhost:10011/incoming-students",
          ws: true
      },
        https: true
      });

    // add browserSync.reload to the tasks array to make
    // all browsers reload after tasks are complete.
    gulp.watch(src.react, ['js-watch']);
    gulp.watch(src.sass, ['sass']);
});
