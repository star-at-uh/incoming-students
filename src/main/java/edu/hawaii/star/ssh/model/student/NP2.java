package edu.hawaii.star.ssh.model.student;

import edu.hawaii.star.dao.sqlserver.exception.NotEnoughResultSetsException;
import edu.hawaii.star.dao.sqlserver.exception.ResultSetNullException;
import edu.hawaii.star.dao.sqlserver.Bind;
import edu.hawaii.star.dao.sqlserver.DB;
import edu.hawaii.star.dao.sqlserver.NamedProc;
import edu.hawaii.star.dao.sqlserver.*;
import edu.hawaii.star.model.SQLModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.Column;
import javax.sql.DataSource;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.sql.*;
import java.util.*;

public class NP2 extends NamedProc {

  /**
   * Serial ID.
   */
  private static final long serialVersionUID = -5695046397599337470L;

  public static final Logger logger = LoggerFactory.getLogger(NP2.class);

  private static Map<String, Map<String, List<String>>> map = new HashMap<String, Map<String, List<String>>>(1000);

  DB database;
  String procName;

  private InitialContext context = null;
  private Connection connection = null;
  private CallableStatement cs = null;
  private int resultSetNumber = 0;
  private boolean mappedParams = false;
  private Map<String, Object> params;

  /**
   * Gets this NP2.
   *
   * @param database {@link DB}
   * @param procName {@link String}
   * @return {@link NP2}
   * @throws NullPointerException Thrown when the proc name is empty.
   */
  public static NP2 get(DB database, String procName, boolean mappedParams) throws NullPointerException {
    NP2 proc = new NP2();

    // Checks the database.
    if (database == null) {
      throw new NullPointerException("The database name must be specified.");
    }

    // trims and checks whether the proc is invalid.
    if (procName == null || (procName = procName.trim()).isEmpty()) {
      throw new NullPointerException("The proc name must be specified.");
    }

    proc.database = database;
    proc.procName = procName;

    proc.mappedParams = mappedParams;
    if (mappedParams) {
      proc.params = new HashMap<String, Object>();
    }

    return proc;
  }

  public static NP2 get(DB database, String procName) throws NullPointerException {
    return get(database, procName, false);
  }

  public DB getDatabase() {
    return database;
  }

  public String getProcName() {
    return procName;
  }

  public void put(String paramName, Object value) {
    if (mappedParams) {
      params.put(paramName, value);
    }
  }

  /**
   * Execute procs with multiple callbacks where there exists one for each result set.
   *
   * @throws SQLException Thrown when the SQL database fails.
   */
  public void execute(Bind<?>... callbacks) throws SQLException, ResultSetNullException, NotEnoughResultSetsException {
    try {
      this.executeProc();

      for (resultSetNumber = 0; resultSetNumber < callbacks.length; resultSetNumber += 1) {
        // break if there are no more results sets available.
        // happens if more callbacks are defined than results sets available.
        if (resultSetNumber > 0 && !cs.getMoreResults()) {
          throw new NotEnoughResultSetsException(this, callbacks.length, resultSetNumber);
        }

        final Bind<? extends SQLModel> callback = callbacks[resultSetNumber];

        ResultSet rs = cs.getResultSet();
        try {
          if (rs == null) {
            throw new ResultSetNullException(this);
          }

          while (rs.next()) {
            callback.bindModel(rs);
          }
        }
        catch (NoSuchMethodException e) {
          // Java Bean get/set paradigm was violated. Usually all models will fail.
          String msg = String
                  .format("Failed to create an instance of %s", callback.getClazz().getCanonicalName());
          logger.warn(msg, e);
        }
        catch (InstantiationException e) {
          String msg = String
                  .format("Failed to create an instance of %s", callback.getClazz().getCanonicalName());
          logger.warn(msg, e);
        }
        catch (IllegalAccessException e) {
          String msg = String
                  .format("Failed to create an instance of %s", callback.getClazz().getCanonicalName());
          logger.warn(msg, e);
        }
        catch (InvocationTargetException e) {
          String msg = String
                  .format("Failed to create an instance of %s", callback.getClazz().getCanonicalName());
          logger.warn(msg, e);
        }
        catch (SQLException e) {
          String msg = String.format("Failed to create an instance of %s on row %s",
                  callback.getClazz().getCanonicalName(), rs.getRow());
          logger.error(msg, e);
          throw e;
        }
        finally {
          if (rs != null) {
            rs.close();
          }
        }
      }
    }
    finally {
      this.close();
    }
  }

  /**
   * @return ResultHandler
   * @throws SQLException Thrown when the SQL database fails.
   */
  public ResultHandler execute() throws SQLException {
    try {
      this.executeProc();
      return this.getResultHandler();
    }
    finally {
      this.close();
    }
  }

  /**
   * Execute procs.
   *
   * @throws SQLException Thrown when the SQL database fails.
   */
  private void executeProc() throws SQLException {

    // get database information.
    String preparedCall = getPreparedProc();
    String debugStatement = getSQLStatement();

    try {
      // get connection from pool
      context = new InitialContext();
      DataSource ds = (DataSource) context.lookup(database.getJDNIReference());
      connection = ds.getConnection();

      if (logger.isTraceEnabled()) {
        String msg = String
            .format("Does SQL Connection %s [%s] exists? %s", database.getDatabaseName(),
                database.getJDNIReference(), (connection != null));
        logger.debug(msg);
      }

      if (logger.isDebugEnabled()) {
        String msg = String.format("Prepared Statement:\n%s", debugStatement);
        logger.debug(msg);
      }

      // execute the query
      cs = connection.prepareCall(preparedCall);
      handleParameters(cs);
      cs.execute();
    }
    catch (NamingException e) {
      String msg = String.format("Enable SQL Connection Pooling for %s", database.getJDNIReference());
      throw new SQLException(msg, e);
    }
    catch (SQLException e) {

      StringBuilder sb = new StringBuilder();
      sb.append("Failed to execute proc\n");
      sb.append(debugStatement);

      throw new SQLException(sb.toString(), e);
    }
  }

  public void close() throws SQLException {
    if (cs != null) {
      cs.close();
    }
    if (connection != null) {
      connection.close();
    }
    if (context != null) {
      try {
        context.close();
      }
      catch (NamingException e) {
        String msg = String.format("Failed to close SQL connection for %s (%s)", getProcName(),
            getDatabase().getDatabaseName());
        throw new SQLException(msg, e);
      }
    }
  }

  private ResultHandler getResultHandler() throws SQLException {
    if (resultSetNumber > 0) {
      if (!cs.getMoreResults()) {
        return null;
      }
    }
    resultSetNumber++;

    ResultSet rs = cs.getResultSet();
    ResultHandler handler = new ResultHandler(rs);
    return handler;
  }

    // new shit
    // 
    // 
    // 

    
/**
   * @return ResultHandler
   * @throws SQLException Thrown when the SQL database fails.
   */
  public ArrayList<ResultHandler> execute2() throws SQLException {
    try {
      this.executeProc();
      return this.getResultHandlers();
    }
    finally {
      this.close();
    }
  }
  private ArrayList<ResultHandler> getResultHandlers() throws SQLException {
    
    ArrayList<ResultHandler> resHandlers = new ArrayList<ResultHandler>();

    while(cs.getMoreResults()) {
        ResultSet rs = cs.getResultSet();
        ResultHandler handler = new ResultHandler(rs);
        resHandlers.add(handler);
    } 

    return resHandlers;
  }

  /**
   * Generates the prepared proc call.
   *
   * @return {@link String}
   */
  private String getPreparedProc() {
    StringBuilder sb = new StringBuilder();
    if (mappedParams) {
      params.values().size();
      int i = 0;
      for (Map.Entry<String, Object> param : params.entrySet()) {
        if (i++ > 0) {
          sb.append(',');
        }
        sb.append("@").append(param.getKey()).append("=?");
      }
    } else {
      for (int i = 0; i < this.size(); i += 1) {
        if (i > 0) {
          sb.append(',');
        }
        sb.append('?');
      }
    }

    String bindVars = sb.toString();
    return String.format("{call %s (%s)}", this.procName, bindVars);
  }

  /**
   * Adds the parameters to the callable statement.
   *
   * @param cs {@link CallableStatement}
   */
  private void handleParameters(CallableStatement cs) throws SQLException {
    if (mappedParams) {
      int i = 1;
      for (Map.Entry<String, Object> param : params.entrySet()) {
        cs.setObject(i++, param.getValue());
      }
    } else {
      for (int i = 1; i <= this.size(); i += 1) {
        Object param = this.get(i - 1);
        cs.setObject(i, param);
      }
    }
  }

  /**
   * Generates the SQL statement for the database management interface.
   *
   * @return {@link String} SQL code that can be run directly in management interface.
   */
  public String getSQLStatement() {
    StringBuilder sb = new StringBuilder();

    sb.append("\t--").append(database.getDatabaseName()).append(" [");
    sb.append(database.getJDNIReference()).append("]\n");
    sb.append('\t').append("exec ");
    sb.append(procName).append('\n');

    if (mappedParams) {
      int i = 0;
      for (Map.Entry<String, Object> param : params.entrySet()) {
        if (i++ > 0) {
          sb.append(",\n");
        }
        sb.append("\t");
        sb.append("@").append(param.getKey());
        sb.append(" = ").append("\'").append(param.getValue()).append("\'");
      }
    } else {
      for (int i = 0; i < this.size(); i += 1) {
        if (i > 0) {
          sb.append(",\n");
        }
        String val = String.valueOf(this.get(i));
        sb.append("\t\'").append(val).append('\'');
      }
    }
    return sb.toString();
  }

  public static Map<String, Map<String, List<String>>> getMap() {
    return map;
  }

  public static Map<String, List<String>> getColumnFieldMap(
      Class<? extends SQLModel> model) {

    Map<String, Map<String, List<String>>> map = getMap();
    String className = model.getCanonicalName();
    Map<String, List<String>> fields = map.get(className);
    if (fields != null) {
      return fields;
    }
    fields = new HashMap<String, List<String>>();
    for (Field field : model.getDeclaredFields()) {
      String fieldName = field.getName();
      if (!Modifier.isStatic(field.getModifiers()) && !Modifier.isFinal(field.getModifiers())) {
        Annotation[] annotations = field.getDeclaredAnnotations();
        if (annotations != null) {
          for (Annotation annotation : annotations) {
            boolean isColumnAnnotation = Column.class.equals(annotation.annotationType());
            if (isColumnAnnotation) {
              Column colAnnotation = (Column) annotation;
              String key = colAnnotation.name();
              List<String> list = fields.get(key);
              if (list == null) {
                list = new LinkedList<String>();
                fields.put(key, list);
              }
              list.add(fieldName);
              break;
            }
          }
        }
      }
    }
    // Doesn't matter if model definition is overwritten.
    // Ensure that we don't block multithreaded read on map.
    map.put(className, fields);
    return fields;
  }
}
