package edu.hawaii.star.ssh.model;

import java.util.HashMap;

public class GeneralResponse extends HashMap<String, Object> {

  private static final String STATUS = "status";
  private static final String MESSAGE = "message";

  public GeneralResponse(boolean status, String message) {
    super();

    this.put(STATUS, status);
    this.put(MESSAGE, message);
  }

  public boolean getStatus() {
    return (Boolean) this.get(STATUS);
  }

  public void setStatus(boolean status) {
    this.put(STATUS, status);
  }

  public String getMessage() {
    return (String) this.get(MESSAGE);
  }

  public void setStatus(String message) {
    this.put(MESSAGE, message);
  }
}
