package edu.hawaii.star.ssh.model.debug;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by breti on 2/6/2015.
 */
public class DebugStatements<T> extends ArrayList<T> {

  private int maxCapacity;
  private List<Long> timeStamps;

  public DebugStatements(int maxCapacity) {
    this.maxCapacity = maxCapacity;
    timeStamps = new ArrayList<Long>();
  }

  @Override
  public boolean add(T t) {
    synchronized (this) {
      if (this.size() >= maxCapacity && this.size() > 0) {
        this.remove(0);
      }
      if (timeStamps.size() >= maxCapacity && timeStamps.size() > 0) {
        timeStamps.remove(0);
      }

      timeStamps.add(System.currentTimeMillis());
      return super.add(t);
    }
  }

  public Date getTime(int index) {
    if (timeStamps == null || timeStamps.size() <= index) {
      return null;
    }
    Long time = timeStamps.get(index);
    return new Date(time);
  }
}
