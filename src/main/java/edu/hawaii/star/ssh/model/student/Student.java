package edu.hawaii.star.ssh.model.student;

import java.sql.SQLException;
import java.util.SortedMap;
import java.util.List;
import java.util.ArrayList;

import edu.hawaii.star.dao.sqlserver.DB;
import edu.hawaii.star.dao.sqlserver.NamedProc;
import edu.hawaii.star.dao.sqlserver.Bind;
import edu.hawaii.star.dao.sqlserver.*;
import edu.hawaii.star.ssh.model.student.NP2;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class Student {

	private static Gson gson = new GsonBuilder().create();

    public static String get(String lastName, String middleName, String firstName, String phone, String email,
            String dob, String ss) throws SQLException {
        NP2 proc = NP2.get(DB.ADVISOR, "ReturningAdult_SubmitIdentifyAsUHStudent_proc");
        proc.add(email);
        proc.add(ss);
        proc.add(dob);
        proc.add(phone);
        proc.add(lastName);
        proc.add(middleName);
        proc.add(firstName);
        
        String gsonPayload;
        ArrayList<ResultHandler> results = proc.execute2();
        // System.out.println(proc.getSQLStatement());
        if(results.size()==0){
            gsonPayload = gson.toJson(proc.execute().getRows());
        } else {
            gsonPayload = gson.toJson(results.get(results.size()-1).getRows());
        }
        return gsonPayload;

  }

    public static SortedMap[] moreInfo(String pidm, int campus, int programCampus, int school, String degree, int major, int semester, String comments, String phone, String email) throws SQLException {
        NamedProc proc = NamedProc.get(DB.ADVISOR, "ReturningAdult_SubmitRequestUHCorrespondance_proc");
        proc.add(pidm);
        proc.add(campus);
        proc.add(programCampus);
        proc.add(school);
        proc.add(degree);
        proc.add(major);
        proc.add(semester);
        proc.add(comments);
        proc.add(phone);
        proc.add(email);
        
        return proc.execute().getRows();

    }

    public static SortedMap[] getSectors(int campus) throws SQLException {
        NamedProc proc = NamedProc.get(DB.ADVISOR, "ReturningAdult_ViewSectorsForInstiution_proc");
        proc.add(campus);

        return proc.execute().getRows();

    }
    

    public static SortedMap[] chooseSector(String pidm, int sector, int campus) throws SQLException {
        NP2 proc = NP2.get(DB.ADVISOR, "ReturningAdult_SubmitStudentSector_proc");
        proc.add(pidm);
        proc.add(sector);
        proc.add(campus);

        ArrayList<ResultHandler> results = proc.execute2();

        return results.get(results.size()-2).getRows();

    }
}
