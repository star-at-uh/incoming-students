package edu.hawaii.star.ssh.model.student;

public class StudentParams {

    private String email;

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    };

    private String ss;

    public String getSs() {
        return this.ss;
    }

    public void setSs(String ss) {
        this.ss = ss;
    };

    private String dob;

    public String getDob() {
        return this.dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    };

    private String phone;

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    };

    private String lastName;

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    };

    private String middleName;

    public String getMiddleName() {
        return this.middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    };

    private String firstName;

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    };

    private String pidm;

    public String getPidm() {
        return this.pidm;
    }

    public void setPidm(String pidm) {
        this.pidm = pidm;
    }

    private Boolean origProgInd;

    public Boolean getOrigProgInd() {
        return this.origProgInd;
    }

    public void isOrigProgInd(Boolean origProgInd) {
        this.origProgInd = origProgInd;
    }
    
    private String comments;

    public String getComments() {
        return this.comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    private int campus;

    public int getCampus() {
        return this.campus;
    }

    public void setCampus(int campus) {
        this.campus = campus;
    }

    private int programCampus;

    public int getProgramCampus() {
        return this.programCampus;
    }

    public void setProgramCampus(int programCampus) {
        this.programCampus = programCampus;
    }

    private String programDegree;

    public String getProgramDegree() {
        return this.programDegree;
    }

    public void setProgramDegree(String programDegree) {
        this.programDegree = programDegree;
    }

    public int programMajor;

    public int getProgramMajor() {
        return this.programMajor;
    }

    public void setProgramMajor(int programMajor) {
        this.programMajor = programMajor;
    }

    private int programSchool;

    public int getProgramSchool() {
        return this.programSchool;
    }

    public void setProgramSchool(int programSchool) {
        this.programSchool = programSchool;
    }

    private int semester;

    public int getSemester() {
        return this.semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    private int sectorKey;

    public int getSectorKey() {
        return this.sectorKey;
    }

    public void setSectorKey(int sectorKey) {
        this.sectorKey = sectorKey;
    }

    private String public_pin;

    public String getPublic_pin() {
        return this.public_pin;
    }

    public void setPublic_pin(String public_pin) {
        this.public_pin = public_pin;
    }

}
