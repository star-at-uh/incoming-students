package edu.hawaii.star.ssh.model.session;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import edu.hawaii.star.ssh.model.debug.DebugStatements;

public class SessionManager {

  public static SessionManager get(HttpServletRequest request) {
    return new SessionManager(request);
  }

  private ServletContext context;
  private HttpSession session;
  private int startTimeInterval = 15;

  private SessionManager(HttpServletRequest request) {
    session = request.getSession();
    context = request.getServletContext();
  }

  public void addDebugStatement(String debug) {
    DebugStatements<String> debugStatements = getDebugStatements();
    if (debugStatements == null) {
      debugStatements = new DebugStatements<String>(60);
      session.setAttribute("debugStatements", debugStatements);
    }
    debugStatements.add(debug);
  }

  public DebugStatements<String> getDebugStatements() {
    return (DebugStatements<String>) session.getAttribute("debugStatements");
  }

  public void logout() {
    session.invalidate();
  }

  public String getLatestModified() {
    return (String) session.getAttribute("latestModifiedDate");
  }

  public void setLatestModified(String date) {
    session.setAttribute("latestModifiedDate", date);
  }


  public String getApplicationType() {
    return context.getInitParameter("applicationType");
  }

  public int getStartTimeInterval() {
    return startTimeInterval;
  }

  public void setStartTimeInterval(int startTimeInterval) {
    this.startTimeInterval = startTimeInterval;
  }
}
