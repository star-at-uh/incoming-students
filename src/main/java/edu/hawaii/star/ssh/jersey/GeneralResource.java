package edu.hawaii.star.ssh.jersey;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
// import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.hawaii.star.ssh.model.student.StudentParams;
import edu.hawaii.star.ssh.model.student.Student;

@Path("")
public class GeneralResource {

  private static Gson gson = new GsonBuilder().create();

  @Context
  private HttpServletRequest request;

  @POST
  @Path("student-search")
  @Produces(MediaType.APPLICATION_JSON)
  public String studentSearch(String body) throws SQLException {
    StudentParams params = gson.fromJson(body, StudentParams.class);
    String json = Student.get(params.getLastName(), params.getMiddleName(), params.getFirstName(), params.getPhone(), params.getEmail(), params.getDob(), params.getPublic_pin());
    return json;
  }

  @POST
  @Path("more-info")
  @Produces(MediaType.APPLICATION_JSON)
  public String moreInfo(String body) throws SQLException {
    StudentParams params = gson.fromJson(body, StudentParams.class);
    return gson.toJson(Student.moreInfo(params.getPidm(), params.getCampus(), params.getProgramCampus(), params.getProgramSchool(), params.getProgramDegree(), params.getProgramMajor(), params.getSemester(), params.getComments(), params.getPhone(), params.getEmail()));
  }

  @POST
  @Path("get-sectors")
  @Produces(MediaType.APPLICATION_JSON)
  public String getSectors(String body) throws SQLException {
    StudentParams params = gson.fromJson(body, StudentParams.class);
    
    return gson.toJson(Student.getSectors(params.getCampus()));
  }

  @POST
  @Path("choose-sector")
  @Produces(MediaType.APPLICATION_JSON)
  public String chooseSector(String body) throws SQLException {
    StudentParams params = gson.fromJson(body, StudentParams.class);
    
    return gson.toJson(Student.chooseSector(params.getPidm(), params.getSectorKey(), params.getProgramCampus()));
  }
}
