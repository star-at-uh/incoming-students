<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page
  import="java.util.*,java.sql.*"
  import="com.google.gson.*"
  import="edu.hawaii.star.model.*"
  import="edu.hawaii.star.model.degree.*"
  import="edu.hawaii.star.model.planner.*"
  import="edu.hawaii.star.model.planner.semester.*"
  import="edu.hawaii.star.model.planner.sandbox.requirement.*"
  import="edu.hawaii.star.model.planner.semester.requirement.*"
  import="edu.hawaii.star.model.user.*"
  import="edu.hawaii.star.model.autoadmissions.*"
  import="edu.hawaii.star.dao.sqlserver.*,edu.hawaii.star.dao.sqlserver.procedure.*"
  import="edu.hawaii.star.ssh.model.session.*"
%>
<%
  response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
  response.setHeader("Pragma","no-cache"); //HTTP 1.0
  response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%
  SessionManager sessionManager = SessionManager.get(request);
%>