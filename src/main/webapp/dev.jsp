<%@ page import="java.util.*, java.lang.Exception, com.google.gson.Gson, edu.hawaii.star.ssh.model.session.SessionManager, java.util.regex.Matcher, java.util.regex.Pattern"%>
<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%!
public void filter(SessionManager sessionmanager, ArrayList<String []> arr, int seconds ) {
  Date now = new Date();
  if (sessionmanager.getDebugStatements() != null) {
    int length = sessionmanager.getDebugStatements().size();

    for (int i = length - 1; i >= 0; i--) {
      Date date = sessionmanager.getDebugStatements().getTime(i);

      if (now.getTime() - date.getTime() <= seconds*1000) {
        String time = (date != null) ? date.toString() : "";

        String statement = sessionmanager.getDebugStatements().get(i);
        statement = statement.replaceAll("\t", "");
        statement = statement.replaceAll("'null'", "null");
        String [] temp = statement.split("]", 0);
        String [] split = statement.split("\n", 0);
        String [] name = split[1].split(" ", 0);
        String [] pair = {name[1], temp[1]};
        arr.add(pair);
      }
    }
  }
}
%>

<html>
  <head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.10/semantic.min.css">
  	<style type="text/css">

      body {
        background-color: #FFC8C8;
      }

      .info {
        display: none;
      }

      .container {
        margin-left: 2vw;
        margin-top: 2vh;
        margin-bottom: 2vh;
        font-size: 20px;
        color: #B7588E;
      }

      .ui.grid {
        margin-left: 5%;
      }

      .code {
        background-color: black;
        color: #FFA1A1;
      }

      .ui.code.segment {
        color: #B7588E;
        background-color: #FFA1A1;
      }
      .ui.back.segment {
        background-color: #FFE1C8;
      }

      .ui.inverted.green.button {
        color: white;
        margin: 5px;
      }

      .ui.inverted.red.button {
        color: white;
      }

      .my-textarea {
        background-color: #333;
        color: #fff;
        font-family: monospace;
        font-size: 14px;
        line-height: 1;
        margin-bottom: 10px;
        padding: 5px 10px;
        width: 100%;
        max-width: 100%;
        min-height: 200px;
        display: none;
      }
    </style>

  <script>
    function copy(id) {
    /* Get the text field */
    /* Select the text field */
    id.select();

    /* Copy the text inside the text field */
    document.execCommand("copy");
    };

    function toggle(id) {

      if (id.style.display == '' || id.style.display == 'none') {
        id.style.display = 'block';
      }
      else if (id.style.display == 'block') {
        id.style.display = 'none';
      }
    }

  </script>

<%
  ArrayList<String []> five = new ArrayList<String []>();
  ArrayList<String []> thirty = new ArrayList<String []>();
  ArrayList<String []> minute = new ArrayList<String []>();

  SessionManager sessionmanager = (SessionManager) SessionManager.get(request);

  filter(sessionmanager, five, 5);
  filter(sessionmanager, thirty, 30);
  filter(sessionmanager, minute, 60);

%>

  </head>
  <body>

    <div class="container"> <h3>Dev Console</h3>
    <p class="lead">Latest proc execution is on top. <a href="">Refresh page.</a></p></div>

    <div class="ui grid">

      <div class="ui five wide column">
        <div class="ui back segment">
          <%
          for (int i = 0; i<five.size(); i++) {
            String id = "five"+i;

            out.println("<div class='ui code segment'>" + five.get(i)[0] + " " +"<button class='ui inverted green button' onclick='copy(" + id + ")'>Copy</button> <button class='ui inverted red button' onclick='toggle("+ id +")'>Show/Hide</button> </div>");
            out.println("<textarea class='my-textarea' readonly id='" + id + "'>" + five.get(i)[1] + "</textarea>");
          }
          %>
        </div>
      </div>

      <div class="ui five wide column">
        <div class="ui back segment">
          <%
          for (int i = 0; i<thirty.size(); i++) {
            String id = "thirty"+i;

            out.println("<div class='ui code segment'>" + thirty.get(i)[0] + " " +"<button class='ui inverted green button' onclick='copy(" + id + ")'>Copy</button> <button class='ui inverted red button' onclick='toggle("+ id +")'>Show/Hide</button> </div>");
            out.println("<textarea class='my-textarea' readonly id='" + id + "'>" + thirty.get(i)[1] + "</textarea>");
          }
          %>
        </div>
      </div>

      <div class="ui five wide column">
        <div class="ui back segment">
          <%
          for (int i = 0; i<minute.size(); i++) {
            String id = "minute"+i;

            out.println("<div class='ui code segment'>" + minute.get(i)[0] + " " +"<button class='ui inverted green button' onclick='copy(" + id + ")'>Copy</button> <button class='ui inverted red button' onclick='toggle("+ id +")'>Show/Hide</button> </div>");
            out.println("<textarea class='my-textarea' readonly id='" + id + "'>" + minute.get(i)[1] + "</textarea>");
          }
          %>
        </div>
      </div>

    </div>
  </body>
</html>
