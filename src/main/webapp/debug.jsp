<%@ page import="java.util.*, java.lang.Exception, com.google.gson.Gson, edu.hawaii.star.ssh.model.session.SessionManager"%>
<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<html>
  <head>
  	<style type="text/css">
      #statements textarea.my-textarea {
        background-color: #333;
        color: #fff;
        font-family: monospace;
        font-size: 14px;
        line-height: 1;
        margin-bottom: 10px;
        padding: 5px 10px;
        width: 100%;
        max-width: 100%;
        min-height: 200px;
      }
  	</style>
    <link href="https://www.star.hawaii.edu/cdn/ajax/libs/bootswatch/3.3.5/paper/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <h3>SQL Debug Statements</h3>
      <p class="lead">Latest proc execution is on top. <a href="">Refresh page.</a></p>
      <div id="statements">
        <%
          SessionManager sessionmanager = (SessionManager) SessionManager.get(request);
          if (sessionmanager.getDebugStatements() != null) {
            int length = sessionmanager.getDebugStatements().size();
            for (int i = length - 1; i >= 0; i--) {
              String statement = sessionmanager.getDebugStatements().get(i);
              Date date = sessionmanager.getDebugStatements().getTime(i);
              String time = (date != null) ? date.toString() : "";
              statement = statement.replaceAll("'null'", "null");
          	  statement = statement.replaceAll("\t", "");
              out.println("<code>" + time + "</code>");
          	  out.println("<textarea class='form-control my-textarea' readonly>" + statement + "</textarea>");
            }
          }
        %>
      </div>
    </div>
  </body>
</html>