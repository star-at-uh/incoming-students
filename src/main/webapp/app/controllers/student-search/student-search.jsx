import React from 'react';
import { Grid, Container } from 'semantic-ui-react';
import $ from 'jquery';

class StudentSearch extends React.Component {
    constructor(props){
        super(props);
        this.selectMonth.bind(this);
        this.state = {
            days: ['Day'],
            years: ['Year']
        }
    }

    componentDidMount(){
        this.props.scrollToSection(1);
        const date = new Date();
        const year = date.getFullYear();
        let yeararray = [];
        yeararray.push('Year');
        for(let i=year; i>=1900; i--){
            yeararray.push(i);
        }
        this.setState({years: yeararray});

        let s1i = document.getElementById('s1i');
        let s2i = document.getElementById('s2i');
        let s3i = document.getElementById('s3i');
        let s4i = document.getElementById('s4i');
        let phi = document.getElementById('phi');
        

        if(this.props.isMobileDevice()){
            phi.addEventListener('touchstart', (e) => {
                phi.type = 'tel';
            });
            s1i.addEventListener('touchstart', (e) => {
                s1i.type = 'tel';
            });
            s2i.addEventListener('touchstart', (e) => {
                s2i.type = 'tel';
            });
            s3i.addEventListener('touchstart', (e) => {
                s3i.type = 'tel';
            });
            s4i.addEventListener('touchstart', (e) => {
                s4i.type = 'tel';
            });
            $("s1i").focusout(function(){
                $(this).type = 'password';
            });
            $("s2i").focusout(function(){
                $(this).type = 'password';
            });
            $("s3i").focusout(function(){
                $(this).type = 'password';
            });
            $("s4i").focusout(function(){
                $(this).type = 'password';
            });
        }
        s1i.onkeydown = (e) => {
            if(!this.isNumber(e.keyCode) && e.keyCode != 8){
                e.preventDefault();
            }
            if(s1i.value.length == 1 && e.keyCode != 8){
                s2i.focus();
                if(this.props.isMobileDevice()) s1i.type = 'password';
            }
        };
        s2i.onkeydown = (e) => {
            
            if(!this.isNumber(e.keyCode) && e.keyCode != 8){
                e.preventDefault();
            }
            if(s2i.value.length == 1 && e.keyCode != 8){
                s3i.focus();
                if(this.props.isMobileDevice()) s2i.type = 'password';
            }
            if(s2i.value.length == 0 && e.keyCode == 8){
                s1i.focus();
                if(this.props.isMobileDevice()) s1i.type = 'tel';
                if(this.props.isMobileDevice()) s2i.type = 'password';
            }
        };
        s3i.onkeydown = (e) => {
            
            if(!this.isNumber(e.keyCode) && e.keyCode != 8){
                e.preventDefault();
            }
            if(s3i.value.length == 1 && e.keyCode != 8){
                s4i.focus();
                if(this.props.isMobileDevice()) s3i.type = 'password';
            }
            if(s3i.value.length == 0 && e.keyCode == 8){
                s2i.focus();
                if(this.props.isMobileDevice()) s2i.type = 'tel';
                if(this.props.isMobileDevice()) s3i.type = 'password';
            }
        };
        s4i.onkeydown = (e) => {
            
            if(!this.isNumber(e.keyCode) && e.keyCode != 8 && e.keyCode != 13){
                e.preventDefault();
            }
            if(s4i.value.length == 0 && e.keyCode == 8){
                s3i.focus();
                if(this.props.isMobileDevice()) s3i.type = 'tel';
                if(this.props.isMobileDevice()) s4i.type = 'password';
            }
            // enter
            if(e.keyCode == 13){
                
                e.preventDefault();
                s4i.blur();
                this.props.submitStudentSearch();
            }
        };

        phi.onkeydown = (e) => {
            this.phone_number_check(phi,e);
        };
    }

    selectMonth(){
        let month = document.getElementById('dmi').value;
        this.days(month);
    }

    days(selectedMonth){
        let days;
        let daysarray = [];
        switch(parseInt(selectedMonth)){
            case 1:
                days = 31;
                break;
            case 2:
                days = 29;
                break;
            case 3:
                days = 31;
                break;
            case 4:
                days = 30;
                break;
            case 5:
                days = 31;
                break;
            case 6:
                days = 30;
                break;
            case 7:
                days = 31;
                break;
            case 8:
                days = 31;
                break;             
            case 9:
                days = 30;
                break;                      
            case 10:
                days = 31;
                break;
            case 11:
                days = 30;
                break;
            case 12:
                days = 31;
                break;
            default:
                
                days = 0;
        }
        daysarray.push('Day');
        for(let i=1; i<=days; i++){
            daysarray.push(i);
        }
        this.setState({days: daysarray});
    }

    // modified
    // https://codepen.io/TalonBauer/pen/lvpBI
    phone_formatting(ele,key_string,restore) {
        var new_number,
            selection_start = ele.selectionStart,
            selection_end = ele.selectionEnd,
            number = ele.value.replace(/\D/g,'');

        if(restore){
            number = number.substring(0,number.length-1);
        } else {
            number = number + key_string;
        }

        if(number.length > 10){
            number = number.substring(0,10);
        }
        
        // automatically add dashes
        if (number.length > 2) {
            // matches: 123 || 123-4 || 123-45
            new_number = '('+number.substring(0,3) + ') ';
            if (number.length === 4 || number.length === 5) {
                // matches: 123-4 || 123-45
                new_number += number.substr(3);
            }
            else if (number.length > 5) {
                // matches: 123-456 || 123-456-7 || 123-456-789
                new_number += number.substring(3,6);
            }
            if (number.length > 6) {
                // matches: 123-456-7 || 123-456-789 || 123-456-7890
                new_number += ('-' + number.substring(6));
            }
        }
        else {
            new_number = number;
        }
    
        ele.value = new_number;
        
    }
    isNumber(k_code){
        return ((k_code >=48 && k_code <=57) || (k_code >=96 && k_code <= 105));
    }    
    // modified
    // https://codepen.io/TalonBauer/pen/lvpBI
    phone_number_check(field,e) {
        var key_code = e.keyCode,
            key_string = String.fromCharCode(key_code),
            press_delete = false,
            dash_key = 189,
            delete_key = [8,46],
            direction_key = [33,34,35,36,37,38,39,40],
            selection_end = field.selectionEnd;

        // used numeric 9-key pad?
        if(!key_string.match(/^\d+$/)){
            key_string = String.fromCharCode(key_code-48)
        }

        if(direction_key.indexOf(key_code) < 0 && key_code != 9){
            e.preventDefault();
            
            // delete key was pressed
            if (delete_key.indexOf(key_code) > -1) {
                press_delete = true;
            }

            // only force formatting is a number or delete key was pressed
            if (key_string.match(/^\d+$/) || this.isNumber(key_code) || press_delete) {
                this.phone_formatting(field,key_string,press_delete);
            }
        }
        
    }

    render(){


        const date = new Date();
        const months = [
            {name: 'Month', value: 0},
            {name: 'January', value: 1},
            {name: 'February', value: 2},
            {name: 'March', value: 3},
            {name: 'April', value: 4},
            {name: 'May', value: 5},
            {name: 'June', value: 6},
            {name: 'July', value: 7},
            {name: 'August', value: 8},
            {name: 'September', value: 9},
            {name: 'October', value: 10},
            {name: 'November', value: 11},
            {name: 'December', value: 12},
        ]
        return (
            <div className={!this.props.isMobileDevice() ? 'section' : 'section-mobile'} style={{backgroundColor: 'white'}} id={this.props.isMobileDevice() ? 'student-search-div-mobile' : ''}>
                {/* for testing */}
                {/* {this.props.isMobileDevice() ? <div>
                offsetHeight: {document.getElementById('landing-div').offsetHeight}
                <br></br>
                clientHeight: {document.getElementById('landing-div').clientHeight}
                <br></br>
                scrollHeight: {document.getElementById('landing-div').offsetHeight}
                <br></br>
                windowHeight: {window.innerHeight}
                </div> : ''} */}
                <div className='section-body' style={{paddingTop: !this.props.isMobileDevice() ? '5vh' : 10, position: 'absolute'}}>
                    <div className={!this.props.isMobileDevice() ? 'section-title' : 'section-title-mobile'}>
                        <h1>Help us to find you in the UH system!</h1>
                        <p>Just fill out some basic info and we'll do the rest</p>
                    </div>
                    {!this.props.isMobileDevice() ? (
                    <Grid>
                        <Grid.Row columns='3'>
                            <Grid.Column className='student-search-column'>
                                <div className='student-search-column'>
                                    <div id='name-info-image' className='student-search-image'></div>
                                    <h1 className='sub-section-title'>Name + Contact</h1>
                                    <input id='fni' className='student-search-input' type='text' placeholder='First Name'/><br/>
                                    <input id='mni' className='student-search-input' type='text' placeholder='Middle Name'/><br/>
                                    <input id='lni' className='student-search-input' type='text' placeholder='Last Name'/><br/>
                                    <input id='phi' className='student-search-input' type='tel' placeholder='(XXX) XXX-XXXX'/><br/>
                                    <input id='emi' className='student-search-input' type='text' placeholder='Email'/><br/>
                                </div>
                            </Grid.Column>
                            <Grid.Column className='student-search-column'>
                                <div className='student-search-column'>
                                    <div id='dob-info-image' className='student-search-image'></div>
                                    <h1 className='sub-section-title'>Date of Birth</h1>
                                    <Grid>
                                        <Grid.Row columns='3'>
                                            <Grid.Column>
                                                <select id='dmi' className='ui select new-select' placeholder='Month' onChange={this.selectMonth.bind(this)}>
                                                    {months.map((month,index) => {
                                                        return (<option key={index} value={month.value}>{month.name}</option>);
                                                    })}
                                                </select>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <select id='ddi' className='ui select new-select'>
                                                    {this.state.days.map((day,index) => {
                                                        return (<option key={index} value={day}>{day}</option>);
                                                    })}
                                                </select>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <select id='dyi' className='ui select new-select'>
                                                    {this.state.years.map((year,index) => {
                                                        return (<option key={index} value={year}>{year}</option>);
                                                    })}
                                                </select>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </div>
                            </Grid.Column>
                            <Grid.Column className='student-search-column'>
                                <div className='student-search-column'>
                                    <div id='ss-info-image' className='student-search-image'></div>
                                    <h1 className='sub-section-title'>Last 4 Digits of SS</h1>
                                    <Grid>
                                        <Grid.Row columns='4'>
                                            <Grid.Column>
                                                <input id='s1i' className='student-search-input centered protected' type='password' pattern="[0-9]" inputmode="numeric" maxLength='1'/>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <input id='s2i' className='student-search-input centered protected' type='password' pattern="[0-9]" inputmode="numeric" maxLength='1'/>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <input id='s3i' className='student-search-input centered protected' type='password' pattern="[0-9]" inputmode="numeric" maxLength='1'/>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <input id='s4i' className='student-search-input centered protected' type='password' pattern="[0-9]" inputmode="numeric" maxLength='1'/>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </div>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    ) : (
                    <div style={{padding: 20}}>
                                <div>
                                    <div id='name-info-image' className='student-search-image'></div>
                                    <h1 className='sub-section-title'>Name + Contact</h1>
                                    <input id='fni' className='student-search-input' type='text' placeholder='First Name'/><br/>
                                    <input id='mni' className='student-search-input' type='text' placeholder='Middle Name'/><br/>
                                    <input id='lni' className='student-search-input' type='text' placeholder='Last Name'/><br/>
                                    <input id='phi' className='student-search-input' type='text' placeholder='(XXX) XXX-XXXX'/><br/>
                                    <input id='emi' className='student-search-input' type='text' placeholder='Email'/><br/>
                                </div>
                                <br/>
                                <div>
                                    <div id='dob-info-image' className='student-search-image'></div>
                                    <h1 className='sub-section-title'>Date of Birth</h1>
                                    <Grid>
                                        <Grid.Row columns='3'>
                                            <Grid.Column>
                                                <select id='dmi' className='new-select' placeholder='Month' onChange={this.selectMonth.bind(this)}>
                                                    {months.map((month,index) => {
                                                        return (<option key={index} value={month.value}>{month.name}</option>);
                                                    })}
                                                </select>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <select id='ddi' className='new-select'>
                                                    {this.state.days.map((day,index) => {
                                                        return (<option key={index} value={day}>{day}</option>);
                                                    })}
                                                </select>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <select id='dyi' className='new-select'>
                                                    {this.state.years.map((year,index) => {
                                                        return (<option key={index} value={year}>{year}</option>);
                                                    })}
                                                </select>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </div>
                                <br/>
                                <br/>
                                <div>
                                    <div id='ss-info-image' className='student-search-image'></div>
                                    <h1 className='sub-section-title'>Last 4 Digits of SS</h1>
                                    <Grid>
                                        <Grid.Row columns='4'>
                                            <Grid.Column>
                                                <input id='s1i' className='student-search-input centered protected' pattern="[0-9]" type='password' maxLength='1'/>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <input id='s2i' className='student-search-input centered protected' pattern="[0-9]" type='password' maxLength='1'/>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <input id='s3i' className='student-search-input centered protected' pattern="[0-9]" type='password' maxLength='1'/>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <input id='s4i' className='student-search-input centered protected' pattern="[0-9]" type='password' maxLength='1'/>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </div>
                                <br/>
                                <br/>
                                <div className='new-next-button-mobile' onClick={() => this.props.submitStudentSearch()}>Submit</div>
                    </div>

                    )}
                </div>
                {!this.props.isMobileDevice() && <div id={'protected-div-1'} style={{width: '100vw', position: 'absolute', textAlign: 'center', marginTop: 'calc(100vh - 100px)'}}>
                    <div className='new-next-button' onClick={() => this.props.submitStudentSearch()}>Submit</div>
                </div>}
            </div>
        );
    }
}

export default StudentSearch;