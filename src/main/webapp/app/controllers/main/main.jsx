import React from 'react';
import 'semantic-ui-css/semantic.css';
import Landing from '../../views/landing/landing.jsx';
import StudentSearch from '../student-search/student-search.jsx';
import StudentInfo from '../../views/student-info/student-info.jsx';
import MapPicker from '../map/map.jsx';
import ChooseCareer from '../career-sector/choose-career.jsx';
import CareerInfo from '../../views/career-sector/career-info.jsx';
import CampusInfo from '../../views/campus-info/campus-info.jsx';
import { Modal, Button, Icon, Header, Dimmer, Loader } from 'semantic-ui-react';
import 'regenerator-runtime';
import axios from 'axios';
import smoothscroll from 'smoothscroll-polyfill';

/* Main parent component */
class Main extends React.Component {
  constructor(props) {
    super(props);
    this.isMobileDevice.bind(this);
    this.scrollToSection.bind(this);
    this.submitStudentSearch.bind(this);
    this.changeCampus.bind(this);
    this.chooseSector.bind(this);
    this.handleOpen.bind(this);
    this.majorConfirm.bind(this);
    this.commentOpen.bind(this);
    this.commentClose.bind(this);
    this.commentConfirm.bind(this);
    this.getSectorsForCampus.bind(this);
    this.chooseProgram.bind(this);
    this.chooseCareerSectorHeight.bind(this);
    this.desktopSection.bind(this);
    this.mobileSection.bind(this);
    this.state = {
      currentSection: 0,
      chosenCampus: null,
      chosenSector: null,
      chosenProgram: null,
      modalOpen: false,
      modalContent: '',
      modalIcon: 'warning sign',
      loading: false,
      orig_major: true,
      sectors: [],
      sectorMajors: []
    };

    // Solution for pausing scroll
    // https://stackoverflow.com/questions/4770025/how-to-disable-scrolling-temporarily
    // left: 37, up: 38, right: 39, down: 40,
    // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
    this.keys = {37: 1, 38: 1, 39: 1, 40: 1};

    this.preventDefault = function(e) {
      e = e || window.event;
      if (e.preventDefault)
          e.preventDefault();
      e.returnValue = false;  
    }

    this.preventDefaultForScrollKeys = function(e) {
        if (this.keys[e.keyCode]) {
          this.preventDefault(e);
            return false;
        }
    }

    this.disableScroll = function() {
      if (window.addEventListener) // older FF
          window.addEventListener('DOMMouseScroll', this.preventDefault, false);
      document.addEventListener('wheel', this.preventDefault, {passive: false}); // Disable scrolling in Chrome
      window.onwheel = this.preventDefault; // modern standard
      window.onmousewheel = document.onmousewheel = this.preventDefault; // older browsers, IE
      window.ontouchmove  = this.preventDefault; // mobile
      document.onkeydown  = this.preventDefaultForScrollKeys;
    }

    this.enableScroll = function() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', this.preventDefault, false);
        document.removeEventListener('wheel', this.preventDefault, {passive: false}); // Enable scrolling in Chrome
        window.onmousewheel = document.onmousewheel = null; 
        window.onwheel = null; 
        window.ontouchmove = null;  
        document.onkeydown = null;  
    }
  }

  
  componentDidMount(){
    // fix mobile scroll
    smoothscroll.polyfill();
    window.__forceSmoothScrollPolyfill__ = true;
    // find out if iphone....ew
    let iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    // set original window height (before keyboard)
    if(this.isMobileDevice()) this.setState({originalMobileInnerHeight: window.innerHeight, iOS: iOS});

    // compat
    if(!this.isMobileDevice()){
      if(window.innerHeight <= 650 || window.innerWidth <= 800){
        let sections = document.getElementsByClassName('section')
        for(let i=0; i< sections.length; i++){
          let sectionItems = sections[i].children;
          for(let j=0; j<sectionItems.length; j++){
            if(sectionItems[j].id.indexOf('protected-div') < 0) sectionItems[j].style.zoom = (window.innerHeight * window.innerWidth) / (650 * 1000);
          }
        }
      }
      window.onresize = function(e){
        if(window.innerHeight <= 650 || window.innerWidth <= 800){
          let sections = document.getElementsByClassName('section')
          for(let i=0; i< sections.length; i++){
            let sectionItems = sections[i].children;
            for(let j=0; j<sectionItems.length; j++){
              if(sectionItems[j].id.indexOf('protected-div') < 0) sectionItems[j].style.zoom = (window.innerHeight * window.innerWidth) / (650 * 1000);
            }
          }
        } else {
          let sections = document.getElementsByClassName('section')
          for(let i=0; i< sections.length; i++){
            let sectionItems = sections[i].children;
            for(let j=0; j<sectionItems.length; j++){
              sectionItems[j].style.zoom = 1;
            }
          }
        }
      }
    }
  }

  
  /* UI FUNCTIONS */
  handleOpen = (content, icon) => this.setState({ modalOpen: true, modalContent: content, modalIcon: icon });

  handleClose = () => this.setState({ modalOpen: false, modalContent: '' });

  commentOpen = (orig_major) => this.setState({ commentOpen: true, orig_major: orig_major });

  commentClose = () => this.setState({ commentOpen: false });
  // https://coderwall.com/p/i817wa/one-line-function-to-detect-mobile-devices-with-javascript
  isMobileDevice() {
    return (typeof window.orientation !== 'undefined') || (navigator.userAgent.indexOf('IEMobile') !== -1);
  };

  async scrollToSection(sectionNumber) {

    if(sectionNumber > this.state.currentSection){

      await this.setState({ currentSection: sectionNumber });
    }
    const sectionHeight = window.innerHeight;

    const finalPos = !this.isMobileDevice() ? 
      this.desktopSection(sectionHeight, sectionNumber)
      // mobile
      : this.mobileSection(sectionNumber, sectionHeight);
    
    window.scrollTo({ top: finalPos, behavior: 'smooth' });
    
    if(sectionNumber == 1){
      // find out if iphone....ew
      let iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
      // set original window height (before keyboard)
      if(this.isMobileDevice()) this.setState({originalMobileInnerHeight: window.innerHeight, iOS: iOS});
    }
  }

  // Cause gary is picky
  chooseCareerSectorHeight(){
    const numberOfSectors = this.state.sectors.length;
    if (numberOfSectors > 8){
      // resize
      const numberExtras = Math.ceil(numberOfSectors/4) - 2;
      const sectorRowHeight = (0.30 * window.innerHeight) + 20;
      return window.innerHeight + (numberExtras * sectorRowHeight);
    } else {
      return window.innerHeight;
    }
  }
  mobileChooseCareerHeight(windowHeight){
    // 200 = 150 for title 50px bottom margin
    let suggestedHeight = 200 + (170 * Math.ceil(this.state.sectors.length / 2));
    return windowHeight > suggestedHeight ? windowHeight : suggestedHeight ;
  }

  desktopSection(sectionHeight, sectionNumber){
    const innerWidth = window.innerWidth;
    if(sectionNumber >= 6){
      return ((sectionHeight * (sectionNumber - 2)) + sectionHeight  + this.chooseCareerSectorHeight());
    } else {
      return (sectionHeight * sectionNumber);
    }
  }

  mobileSection(sectionNumber, windowHeight){
    let finalHeight = 0;
    // landing is 100vh
    finalHeight = sectionNumber >= 1 ? finalHeight + document.getElementById('landing-div').offsetHeight : finalHeight;
    // #student-search-div-mobile{ height: 1150px !important; }
    finalHeight = sectionNumber >= 2 ? finalHeight + 1300 : finalHeight;
    // #student-info-div-mobile{ height: 830px !important; }
    finalHeight = sectionNumber >= 3 ? finalHeight + 1000 : finalHeight;
    // #map-div-mobile{ height: 650px !important; }
    finalHeight = sectionNumber >= 4 ? finalHeight + 900 : finalHeight; 
    // campus info part
    finalHeight = sectionNumber >= 5 ? finalHeight + 900 : finalHeight;
    // #choose-career-div-mobile{ height: 650px !important; }
    if(sectionNumber >= 6){
      
      finalHeight += this.mobileChooseCareerHeight(windowHeight);
    }
    // #career-info-div-moble{ height: 830px !important; }
    finalHeight = sectionNumber >= 7 ? finalHeight + 986 : finalHeight;

    return finalHeight;
  }

  // map stuff
  async initMap() {
    await this.setState({ currentSection: 3 });
    this.scrollToSection(3);
  }

  changeCampus(campus) {
    this.setState({ chosenCampus: campus }, ()=> {
      // if(this.isMobileDevice()){
        this.getSectorsForCampus();
      // }
    });
  }

  /* 


  PART 1: STUDENT SEARCH 
  
  
  
  */

  async submitStudentSearch() {
    await this.setState({ currentSection: 1 });
    let fn = document.getElementById('fni').value;
    let ln = document.getElementById('lni').value;
    let mn = document.getElementById('mni').value;
    let ph = document.getElementById('phi').value;
    let em = document.getElementById('emi').value;
    let dm = document.getElementById('dmi').value;
    let dd = document.getElementById('ddi').value;
    let dy = document.getElementById('dyi').value;
    let s1i = document.getElementById('s1i').value;
    let s2i = document.getElementById('s2i').value;
    let s3i = document.getElementById('s3i').value;
    let s4i = document.getElementById('s4i').value;
    
    // gonna hardcode values for now, uncomment ajax call later
    this.setState({ currentSection: 2,
      student: {
        pidm: 1561359,
        percentComplete: 77,
        currentContact: {
            lastName: 'Boiz',
            middleName: '',
            firstName: 'STAR',
            phone: '(808) 000-0000',
            email: 'starhelp@hawaii.edu'
        }
      }
    }, () => {
      this.enableScroll();
      this.scrollToSection(2);
    });
    
  

    // if(fn && ln && ph && em && dm && dd && dy && s1i && s2i && s3i && s4i){
    //   const dob = `${dy}-${dm < 10 ? '0' : ''}${dm}-${dd < 10 ? '0' : ''}${dd} 00:00:00`;
    //   this.setState({loading: true}, this.disableScroll);
    //   axios.post('api/student-search',{
    //     lastName: ln,
    //     middleName: mn == null ? '' : mn,
    //     firstName: fn,
    //     phone: ph,
    //     email: em,
    //     dob: dob,
    //     public_pin: `${s1i}${s2i}${s3i}${s4i}`
    //   }).then((response)=>{
    //     if(response.data.length > 0 && response.data[0].Status != 0){
    //       const student = response.data[0];
    //       this.setState({ currentSection: 2,
    //         student: {
    //           id: student.StudentID,
    //           firstName: student.ReturningAdultFirstName,
    //           lastName: student.ReturningAdultLastName,
    //           email: student.StudentUHEmail,
    //           credits: student.ReturningAdultCREarned,
    //           gpa: student.ReturningAdultGPA,
    //           semester: student.ReturningAdultSemesterKey,
    //           program: student.ReturningAdultProgramName,
    //           campusKey: student.ReturningAdultCampusKey,
    //           schoolKey: student.ReturningAdultSchoolKey,
    //           majorKey: student.ReturningAdultMajorKey,
    //           degreeKey: student.ReturningAdultDegreeKey,
    //           pidm: student.ReturningAdultPidm,
    //           percentComplete: student.ReturningAdultPercentComplete,
    //           fullMajorName: student.FullMajorName,
    //           currentContact: {
    //             lastName: ln,
    //             middleName: mn,
    //             firstName: fn,
    //             phone: ph,
    //             email: em
    //           }
    //         },
    //         loading: false
    //       }, () => {
    //         this.enableScroll();
    //         this.scrollToSection(2);
    //       }
    //       );
    //     } else {
    //       this.setState({loading: false}, this.enableScroll);
    //       if(response.data.length > 0 && response.data[0].Status == 0){ 
    //         let messageString = `${response.data[0].Message}\n\n
    //           You entered the following:\n
    //           First Name: ${fn}\n
    //           Last Name: ${ln}\n
    //           Date of Birth: ${dy}-${dm < 10 ? '0' : ''}${dm}-${dd < 10 ? '0' : ''}${dd}\n
    //           Last 4 SSN: ****

    //         `
    //         this.handleOpen(messageString, 'search');
    //       } else {
    //         this.handleOpen('Could not find any records, please double check your information.', 'search');
    //       }
    //     }
    //   });
    // } else if(ph.length < 10){
    //   this.handleOpen('Invalid phone number', 'warning sign');
    // } else if(em.indexOf('@') >= em.lastIndexOf('.') || em.indexOf('@') == -1){
    //   this.handleOpen('Invalid email address', 'warning sign');
    // } else if(!(s1i && s2i && s3i && s4i)){
    //   this.handleOpen('Invalid social security number', 'warning sign');
    // } else {
    //   this.handleOpen('Please fill out all required fields', 'warning sign');
    // } 
  }

  
  /* 


  PART 2: GET CAMPUS SECTORS 
  
  
  */


  getSectorsForCampus(){
    axios.post('api/get-sectors',{
      campus: this.state.chosenCampus.id
    }).then((response) => {
      this.setState({sectors: response.data, currentSection: 4}, () => this.scrollToSection(4));
    })
  }
  
  
  /* 


  PART 3: CHOOSE SECTOR 
  
  
  */

  chooseSector(sector) {
    this.setState({loading: true}, this.disableScroll);
    // axios.post('api/choose-sector', {
    //   pidm: this.state.student.pidm,
    //   sectorKey: sector.ReturnAdultProgramSectorKey,
    //   programCampus: this.state.chosenCampus.id}
    // ).then((response) => {
    //   if(response.data.length > 0){
    //     this.setState({ chosenSector: sector, sectorMajors: response.data, loading: false, currentSection: 6 }, () => {
    //       this.enableScroll();
    //       this.scrollToSection(6);
    //     });
    //   } else {
    //     this.setState({ loading: false }, () => {
    //       this.enableScroll();
    //       this.handleOpen('We could not find any programs associated with that career field at this campus.', 'search');
    //     });
    //   }
    // })
    this.setState({ chosenSector: sector, loading: false, currentSection: 6 }, () => {
      this.enableScroll();
      this.scrollToSection(6);
    });
  }

  
  
  /* 


  PART 4: CHOOSE MAJOR
  
  
  */

  chooseProgram(program) {
    this.setState({chosenProgram: program}, () => {
      this.commentOpen(false);
    });
  }

  
  
  /* 


  PART 5: WRITE COMMENTS
  
  
  */

  commentConfirm(){
    let comments = document.getElementById('major-comments').value;
    this.setState({commentOpen: false});
    this.majorConfirm(this.state.orig_major, comments)
  }

  
  
  /* 


  PART 6: SEND DATA
  
  
  */

  majorConfirm(orig_major, comments){
    const student = this.state.student;
    let payload = {};
    let message = '';
    /*currentContact: {
                lastName: ln,
                middleName: mn,
                firstName: fn,
                phone: ph,
                email: em
              }*/
    if(orig_major){
      payload = {
        pidm: student.pidm,
        phone: student.currentContact.phone,
        email: student.currentContact.email,
        campus: student.campusKey,
        semester: student.semester,
        programCampus: student.campusKey,
        programSchool: student.schoolKey,
        programDegree: student.degreeKey,
        programMajor: student.majorKey,
        programSemester: student.semester,
        comments: comments
      };
    } else {
      const program = this.state.chosenProgram;
      payload = {
        pidm: student.pidm,
        phone: student.currentContact.phone,
        email: student.currentContact.email,
        campus: student.campusKey,
        semester: program.ReturnAdultSectorOutcomeSemester,
        programCampus: this.state.chosenCampus.id,
        programSchool: program.ReturnAdultSectorOutcomeSchool,
        programDegree: program.ReturnAdultSectorOutcomeDegree,
        programMajor: program.ReturnAdultSectorOutcomeMajor,
        programSemester: program.ReturnAdultSectorOutcomeSemester,
        comments: comments
      };
    }
    
    // send advisor info about interest chosen major
    axios.post('api/more-info', payload)
    .then((response) =>{
      if(response.data.length > 0){
        message = response.data[0].Message
      } else {
        message = 'We apologize, an unexpected error as occurred. Please try again later';
      }
      this.handleOpen(message.length == 0 ?
        'Your student record has been forwarded to an academic advisor for review and you will be contacted shortly to discuss the next steps!'
        : message, message.length == 0 ? 'send' : 'warning sign');
    }).catch((error) => {
      console.log(error);
      this.handleOpen('We apologize, an unexpected error as occurred. Please try again later', 'warning sign');
    });
    
  }

  loading(){

      return (
        <Dimmer style={{marginTop: this.state.currentSection < 2 && this.isMobileDevice() ?
          this.state.originalMobileInnerHeight + 200 : window.scrollY,
          height: this.state.currentSection < 2 && this.isMobileDevice() ?
          1300 : window.innerHeight}} active>
          <Loader />
        </Dimmer>
      );
      
  }

  render() {
    return (
      <div style={{overflowX: 'hidden'}}>
        {this.state.loading && this.loading()}
        <Landing scrollToSection={this.scrollToSection.bind(this)}  isMobileDevice={this.isMobileDevice.bind(this)}/>
        {this.state.currentSection >= 1 ? <StudentSearch iOS={this.state.iOS} isMobileDevice={this.isMobileDevice.bind(this)} scrollToSection={this.scrollToSection.bind(this)} submitStudentSearch={this.submitStudentSearch.bind(this)} /> : ''}
        {this.state.currentSection >= 2 ? <StudentInfo isMobileDevice={this.isMobileDevice.bind(this)} scrollToSection={this.scrollToSection.bind(this)} student={this.state.student}/> : ''}
        {this.state.currentSection >= 3 ? <MapPicker desktopSection={this.desktopSection.bind(this)} mobileSection={this.mobileSection.bind(this)}
          isMobileDevice={this.isMobileDevice.bind(this)} changeCampus={this.changeCampus.bind(this)} scrollToSection={this.scrollToSection.bind(this)} getSectorsForCampus={this.getSectorsForCampus.bind(this)}/> : ''}
        {this.state.currentSection >= 4 && <CampusInfo campusId={this.state.chosenCampus.id} scrollToSection={this.scrollToSection.bind(this)} isMobileDevice={this.isMobileDevice.bind(this)} />}
        {this.state.currentSection >= 5 ? <ChooseCareer chooseCareerSectorHeight={this.chooseCareerSectorHeight.bind(this)} isMobileDevice={this.isMobileDevice.bind(this)} campus={this.state.chosenCampus} chooseSector={this.chooseSector.bind(this)} sectors={this.state.sectors} scrollToSection={this.scrollToSection.bind(this)} /> : ''}
        {this.state.currentSection >= 6 ? <CareerInfo isMobileDevice={this.isMobileDevice.bind(this)} scrollToSection={this.scrollToSection.bind(this)} career={this.state.chosenSector} />  : ''}

        <Modal
          open={this.state.modalOpen}
          onClose={this.handleClose}
          basic
          size='small'
          closeIcon
          style={{marginTop: '-20vh'}}
        >
          <Header icon={this.state.modalIcon} content={this.state.modalIcon == 'warning sign' ? 'Error' : this.state.modalIcon == 'search' ? 'No Results Found' : 'Sent'} />
          <Modal.Content>
            {this.state.modalContent.split('\n').map((paragraph, index) => {
              return index == 0 ? <h3>{paragraph}</h3> : <p>{paragraph}</p>
            })}
          </Modal.Content>
          <Modal.Actions>
            <Button color='green' onClick={this.handleClose} inverted>
              <Icon name='checkmark' /> Got it
            </Button>
          </Modal.Actions>
        </Modal>
        <Modal
          open={this.state.commentOpen}
          onClose={this.commentClose}
          size='small ish'
          closeIcon
        >
          <Header icon='graduation cap' content='Send your request to an academic advisor' />
          <Modal.Content>
            <h3>Click confirm to have your student record sent to an academic advisor for more information on this degree and the next steps to enroll in a program. Optionally, you may leave a comment below as well.</h3>
            <textarea id='major-comments' style={{height: '150px', width: '100%'}}></textarea>
          </Modal.Content>
          <Modal.Actions>
            <Button color='green' onClick={this.commentConfirm.bind(this)} inverted>
              <Icon name='checkmark' /> Confirm
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }
}

export default Main;