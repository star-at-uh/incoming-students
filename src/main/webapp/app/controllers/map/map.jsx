import React from 'react';
import { Grid, Dropdown } from 'semantic-ui-react';
import { isNull } from 'util';
import { keys } from 'regenerator-runtime';
import $ from 'jquery';

class MapPicker extends React.Component {
    constructor(props){
        super(props);
        this.changeCampusImage.bind(this);
        this.getLocationInit.bind(this);
        this.chooseSegment.bind(this);
        this.state = {
            numCampuses: 0,
            nearbyCampuses: [],
            selectedCampus: {
                code: 'MAN',
                name: 'UH Manoa',
                id: 1,
                img: 'public/images/map2/UHManoa.jpg',
                img_mobile: 'public/images/map/Map_UHManoa_mobile.png',
                lat: 21.296944,
                lng: -157.8193005
            },
            campuses: [
                {
                    code: 'MAN',
                    name: 'UH Manoa',
                    id: 1,
                    // img: 'public/images/map/Map_UHManoa.jpg',
                    img: 'public/images/map2/UHManoa.jpg',
                    img2: 'public/images/map3/UHManoa.png',
                    img_mobile: 'public/images/map/Map_UHManoa_mobile.png',
                    lat: 21.296944,
                    lng: -157.8193005
                },
                {
                    code: 'HIL',
                    name: 'UH Hilo',
                    id: 4,
                    // img: 'public/images/map/Map_UHHilo.jpg',
                    img: 'public/images/map2/UHHilo.jpg',
                    img2: 'public/images/map3/UHHilo.png',
                    img_mobile: 'public/images/map/Map_UHHilo_mobile.png',
                    lat: 19.7010168,
                    lng: -155.0822426
                },
                {
                    code: 'HAW',
                    name: 'Hawai\'i CC',
                    id: 5,
                    // img: 'public/images/map/Map_HawaiiCC.jpg',
                    img: 'public/images/map2/HawaiiCC.jpg',
                    img2: 'public/images/map3/HawaiiCC.png',
                    img_mobile: 'public/images/map/Map_HawaiiCC_mobile.png',
                    lat: 19.7050244,
                    lng: -155.0721412
                },
                {
                    code: 'KAU',
                    name: 'Kaua\'i CC',
                    id: 9,
                    // img: 'public/images/map/Map_KauaiCC.jpg',
                    img: 'public/images/map2/KauaiCC.jpg',
                    img2: 'public/images/map3/KauaiCC.png',
                    img_mobile: 'public/images/map/Map_KauaiCC_mobile.png',
                    lat: 21.968275, 
                    lng: -159.3984906
                },
                {
                    code: 'KAP',
                    name: 'Kapi\'olani CC',
                    id: 2,
                    // img: 'public/images/map/Map_KapiolaniCC.jpg',
                    img: 'public/images/map2/KapCC.jpg',
                    img2: 'public/images/map3/KapiolaniCC.png',
                    img_mobile: 'public/images/map/Map_KapiolaniCC_mobile.png',
                    lat: 21.269376, lng: -157.8039007
                },
                {
                    code: 'HON',
                    name: 'Honolulu CC',
                    id: 3,
                    // img: 'public/images/map/Map_HonoluluCC.jpg',
                    img: 'public/images/map2/HonCC.jpg',
                    img2: 'public/images/map3/HonoluluCC.png',
                    img_mobile: 'public/images/map/Map_HonoluluCC_mobile.png',
                    lat: 21.3204614, lng:-157.8714161
                },
                {
                    code: 'MAU',
                    name: 'Maui College',
                    id: 8,
                    // img: 'public/images/map/Map_MauiCollege.jpg',
                    img: 'public/images/map2/MauiCC.jpg',
                    img2: 'public/images/map3/MauiCollege.png',
                    img_mobile: 'public/images/map/Map_MauiCollege_mobile.png',
                    lat: 20.8893196, lng: -156.4808397
                },
                {
                    code: 'WOA',
                    name: 'UH West O\'ahu',
                    id: 10,
                    // img: 'public/images/map/Map_UHWestOahu.jpg',
                    img: 'public/images/map2/UHWO.jpg',
                    img2: 'public/images/map3/UHWestOahu.png',
                    img_mobile: 'public/images/map/Map_UHWestOahu_mobile.png',
                    lat: 21.355193, lng: -158.0582537
                },
                {
                    code: 'WIN',
                    name: 'Windward CC',
                    id: 7,
                    // img: 'public/images/map/Map_WindwardCC.jpg',
                    img: 'public/images/map2/WinCC.jpg',
                    img2: 'public/images/map3/WindwardCC.png',
                    img_mobile: 'public/images/map/Map_WindwardCC_mobile.png',
                    lat: 21.4079229, lng: -157.8144106
                },
                {
                    code: 'LEE',
                    name: 'Leeward CC',
                    id: 6,
                    // img: 'public/images/map/Map_LeewardCC.jpg',
                    img: 'public/images/map2/LeewardCC.jpg',
                    img2: 'public/images/map3/LeewardCC.png',
                    img_mobile: 'public/images/map/Map_LeewardCC_mobile.png',
                    lat: 21.3926748, lng: -157.9860347
                } 
            ]
        }
    }

    componentDidMount(){
        let currentComponent = this;
        this.props.scrollToSection(3); 
        // commented out code only works for select dropdown version of map picker
        // this.props.changeCampus({
        //     code: 'MAN',
        //     name: 'UH Manoa',
        //     id: 1,
        //     img: 'public/images/map/Map_UHManoa.jpg',
        //     img_mobile: 'public/images/map/Map_UHManoa_mobile.png'
        // });
        this.getLocationInit(currentComponent);
    }

    // Code from : https://www.geodatasource.com/developers/javascript
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    //:::                                                                         :::
    //:::  This routine calculates the distance between two points (given the     :::
    //:::  latitude/longitude of those points). It is being used to calculate     :::
    //:::  the distance between two locations using GeoDataSource (TM) prodducts  :::
    //:::                                                                         :::
    //:::  Definitions:                                                           :::
    //:::    South latitudes are negative, east longitudes are positive           :::
    //:::                                                                         :::
    //:::  Passed to function:                                                    :::
    //:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
    //:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
    //:::    unit = the unit you desire for results                               :::
    //:::           where: 'M' is statute miles (default)                         :::
    //:::                  'K' is kilometers                                      :::
    //:::                  'N' is nautical miles                                  :::
    //:::                                                                         :::
    //:::  Worldwide cities and other features databases with latitude longitude  :::
    //:::  are available at https://www.geodatasource.com                         :::
    //:::                                                                         :::
    //:::  For enquiries, please contact sales@geodatasource.com                  :::
    //:::                                                                         :::
    //:::  Official Web site: https://www.geodatasource.com                       :::
    //:::                                                                         :::
    //:::               GeoDataSource.com (C) All Rights Reserved 2018            :::
    //:::                                                                         :::
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    distance(lat1, lon1, lat2, lon2, unit) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        }
        else {
            var radlat1 = Math.PI * lat1/180;
            var radlat2 = Math.PI * lat2/180;
            var theta = lon1-lon2;
            var radtheta = Math.PI * theta/180;
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180/Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit=="K") { dist = dist * 1.609344 }
            if (unit=="N") { dist = dist * 0.8684 }
            return dist;
        }
    }


    getLocationInit(currentComponent){
        function setPosition(position){
            currentComponent.setState({lat: position.coords.latitude, lng: position.coords.longitude}, ()=>{
                if(position.coords.latitude && position.coords.longitude){
                    currentComponent.setState({
                        nearbyCampuses: currentComponent.state.campuses.sort((a,b) => 
                        currentComponent.distance(a.lat,a.lng,currentComponent.state.lat,currentComponent.state.lng,'M') - 
                        currentComponent.distance(b.lat,b.lng,currentComponent.state.lat,currentComponent.state.lng,'M') 
                    )
                    }, 
                    () => {
                        currentComponent.forceUpdate();
                        //currentComponent.chooseSegment(currentComponent.state.nearbyCampuses[0]);
                    });
                }
           });
        };
        let currentPosition = {};
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(function(position){
                setPosition(position);
            });
        } else { 
            // default to UH Manoa
            setPosition({
                coords: {
                    latitude: null,//21.296939,
                    longitude: null//-157.8193005
                }
            });
        }
    };


    changeCampusImage(){
        let select = document.getElementById(this.props.isMobileDevice() ? 'campus-select-mobile' : 'campus-select');
        if(select.value){
            let campus = this.state.campuses[select.value];
            this.setState({selectedCampus: select.value, selectedCampusImg: this.props.isMobileDevice() ? campus.img_mobile : campus.img});
            this.props.changeCampus(campus);
        }
        this.props.scrollToSection(4); // force mobile to re-align
    }

    initMap(){
        const mapHeight = window.innerHeight / 2;
        const mapWidth = 0
    }

    render(){
        return (
            <div>{this.props.isMobileDevice() ? this.renderMobile() : this.renderDesktop()}</div>
        );
    }

    renderDesktop(){
        return (
            <div className='section' style={{backgroundColor: 'white'}}>
                <div id='map-header-div' className='map-section-title'><h1>Choose a UH Campus</h1>
                {this.state.nearbyCampuses.length > 0 && <p>{
                    this.state.nearbyCampuses.filter((campus) => this.distance(campus.lat, campus.lng, this.state.lat, this.state.lng, 'M') < 50).length > 0 ?
                    this.state.nearbyCampuses.filter((campus) => this.distance(campus.lat, campus.lng, this.state.lat, this.state.lng, 'M') < 50).length : 0
                } campuses nearby</p>}</div>
                <Grid columns={2}>
                    <Grid.Column id='map-choices' style={{width: '40vw', paddingLeft: '100px', paddingTop: '40px', float: 'left'}}>
                        {this.state.nearbyCampuses.slice(0, 3).map((campus) => this.desktopSegment(campus))}
                        <br/><br/>
                        <p style={{
                            color: '#2f2f2f',
                            fontSize: '30px'}}>
                        Select a {this.state.nearbyCampuses.length > 0 && 'different '}campus:
                        </p>
                        <Dropdown id='map-custom-select'
                            placeholder='Choose a campus...'
                            fluid
                            search
                            selection
                            onChange={(e, value) => {this.chooseCampusFromSelect(value)}}
                            options={this.state.campuses.map((campus, index) => {
                                return {
                                    key: index,
                                    text: campus.name,
                                    value: campus
                                };
                            })}
                        />
                    </Grid.Column>
                    <Grid.Column id='map-prev' style={{width: '60vw', height: '70vh', padding: '40px', float: 'right'}}>
                        <div style={{textAlign: 'center', width: 'calc(60vw - 120px)', height: 'calc((60vw - 120px) /3)'}}>
                            <div style={{width: 'calc(60vw - 120px)', height: 'calc((60vw - 120px) /3)', 
                                backgroundRepeat: 'none', backgroundSize: 'cover', position: 'absolute', backgroundImage: `linear-gradient(to right, white,gray,white)`}}>
                                    
                                <img src={this.state.selectedCampus.img} style={{height: 'calc((60vw - 120px) /3)'}}></img>
                            </div>
                        </div>
                        {/* <div class='new-next-button' style={{marginTop: 100}} onClick={() => this.props.getSectorsForCampus()}>Select Campus</div> */}
                    </Grid.Column>
                </Grid>
            </div>
        );
    }

    chooseCampusFromSelect(campus){
        campus = campus.value;

        let sector;

        // reset if selected
        if(!isNull(this.state.selectedCampus.id)){
            sector = document.getElementById(`campus-segment-${this.state.selectedCampus.id}`);
            if(sector){
                sector.classList.remove('map-selected-sector');
                if(!this.props.isMobileDevice()){
                    document.getElementById(`island-icon-${this.state.selectedCampus.id}`).classList.remove('island-adjust');
                    document.getElementById(`campus-name-segment-${this.state.selectedCampus.id}`).classList.remove('campus-name-adjust');
                } else {
                    document.getElementById(`island-icon-${this.state.selectedCampus.id}`).classList.remove('island-adjust-mobile');
                    document.getElementById(`campus-name-segment-${this.state.selectedCampus.id}`).classList.remove('campus-name-adjust-mobile');
                }
            }
        }

        
        this.setState({selectedCampus: campus});
        this.props.changeCampus(campus);
        
        if(this.props.isMobileDevice())this.props.scrollToSection(4); 
    }

    chooseSegment(campus){
        let sector;

        // reset if selected
        if(!isNull(this.state.selectedCampus.id)){
            sector = document.getElementById(`campus-segment-${this.state.selectedCampus.id}`);
            if(sector){
                sector.classList.remove('map-selected-sector');
                if(!this.props.isMobileDevice()){
                    document.getElementById(`island-icon-${this.state.selectedCampus.id}`).classList.remove('island-adjust');
                    document.getElementById(`campus-name-segment-${this.state.selectedCampus.id}`).classList.remove('campus-name-adjust');
                } else {
                    document.getElementById(`island-icon-${this.state.selectedCampus.id}`).classList.remove('island-adjust-mobile');
                    document.getElementById(`campus-name-segment-${this.state.selectedCampus.id}`).classList.remove('campus-name-adjust-mobile');
                }
            }
        }

        // set next
        sector = document.getElementById(`campus-segment-${campus.id}`);
        sector.classList.add('map-selected-sector');
        if(!this.props.isMobileDevice()){
            document.getElementById(`island-icon-${campus.id}`).classList.add('island-adjust');
            document.getElementById(`campus-name-segment-${campus.id}`).classList.add('campus-name-adjust');
        } else {
            document.getElementById(`island-icon-${campus.id}`).classList.add('island-adjust-mobile');
            document.getElementById(`campus-name-segment-${campus.id}`).classList.add('campus-name-adjust-mobile');
        }
        this.setState({selectedCampus: campus});
        this.props.changeCampus(campus);
    }


    desktopSegment(campus){
        return (
            <div id={`campus-segment-${campus.id}`} style={{backgroundColor: '#E8E8E8', height: '100px', width: '30vw', marginBottom: '20px'}}
            onClick={()=> this.chooseSegment(campus)}>
                <Grid columns={2} className={'map-segment-grid'}>
                    <Grid.Column id={`island-icon-${campus.id}`} width={3}>
                        <img src={campus.img2} height='70px' style={{marginLeft: '20px'}}></img>
                    </Grid.Column>
                    <Grid.Column  id={`campus-name-segment-${campus.id}`}
                    width={13} style={{paddingLeft: '30px', paddingTop: '25px'}}>
                        <span style={{fontSize: '25px', lineHeight: '20px'}}>{campus.name}</span><br/>
                        {this.distance(campus.lat, campus.lng, this.state.lat, this.state.lng, 'M').toFixed(1)} miles away
                    </Grid.Column>
                </Grid>
            </div>
        );
    }

    mobileSegment(campus){
        return (
            <div id={`campus-segment-${campus.id}`} style={{backgroundColor: '#E8E8E8', height: '75px', width: '100vw', marginBottom: '10px'}}
            onClick={()=> this.chooseSegment(campus)}>
                <Grid columns={2} className={'map-segment-grid'}>
                    <Grid.Column id={`island-icon-${campus.id}`} width={3}>
                        <img src={campus.img2} height='50px' style={{marginLeft: '10px'}}></img>
                    </Grid.Column>
                    <Grid.Column  id={`campus-name-segment-${campus.id}`}
                    width={13} style={{paddingLeft: '10px', paddingTop: '15px'}}>
                        <span style={{fontSize: '15px', lineHeight: '10px'}}>{campus.name}</span><br/>
                        {this.distance(campus.lat, campus.lng, this.state.lat, this.state.lng, 'M').toFixed(1)} miles away
                    </Grid.Column>
                </Grid>
            </div>
        );
    }

    renderMobile(){
        return (
            <div style={{backgroundColor: 'white', height: '900px'}}>
                <div style={{width: '100vw', height: 'calc(100vw /3)',  
                            backgroundRepeat: 'none', backgroundSize: 'cover', backgroundImage: `linear-gradient(to right, white,gray,white)`,
                            textAlign: 'center'}}>
                                <img src={this.state.selectedCampus.img} style={{height: 'calc(100vw /3)'}}></img>
                            </div>
                <div id='map-header-div' style={{color: '#2f2f2f', marginLeft: '15px'}}><h1>Choose a UH Campus</h1>

                {this.state.nearbyCampuses.length > 0 && <p style={{color: '#2f2f2f', marginLeft: '15px', marginTop: '-10px', marginBottom: '10px'}}>{
                    this.state.nearbyCampuses.filter((campus) => this.distance(campus.lat, campus.lng, this.state.lat, this.state.lng, 'M') < 50).length > 0 ?
                    this.state.nearbyCampuses.filter((campus) => this.distance(campus.lat, campus.lng, this.state.lat, this.state.lng, 'M') < 50).length : 0
                } campuses nearby</p>}</div>

                <div id='map-choices' style={{width: '100vw'}}>
                    {this.state.nearbyCampuses.slice(0, 3).map((campus) => this.mobileSegment(campus))}
                    <br/><br/>
                    <p style={{
                        color: '#2f2f2f',
                        fontSize: '20px',
                        marginLeft: '15px'}}>
                    Select a {this.state.nearbyCampuses.length > 0 && 'different '}campus:
                    </p>
                    <Dropdown id='map-custom-select-mobile'
                        placeholder='Choose a campus...'
                        fluid
                        search
                        selection
                        onChange={(e, value) => {this.chooseCampusFromSelect(value)}}
                        options={this.state.campuses.map((campus, index) => {
                        return {
                            key: index,
                            text: campus.name,
                            value: campus
                        };
                    })}
                    />
                
                </div>
                
                {/* <div class='new-next-button-mobile' style={{float: 'right', marginTop: '20px'}} onClick={() => this.props.getSectorsForCampus()}>Select Campus</div> */}
            </div>
        );
    }

    //saving incase gary changes mind...
    // using for mobile for now
    // renderMobile(){
    //     return (
    //         // <div style={{marginTop: `${this.props.isMobileDevice() ? this.props.mobileSection(4) - 3734 : this.props.desktopSection(4) - 3734}`}}>
    //         <div style={{marginTop: '1rem'}}>
    //             <div style={{width: '100vw', height: this.props.isMobileDevice() ? '45px' : '75px', backgroundColor: 'black', 
    //                 verticalAlign: 'middle', position: 'absolute', zIndex: 888,  opacity: 0.5}}></div>
    //                 <h1 style={{color: 'white', margin: 0, padding: this.props.isMobileDevice() ? '4px 5px 5px 10px' : '19.5px', position: 'absolute', zIndex: 9999}}>Choose a UH campus</h1>
    //             <Grid id='campus-grid' columns={this.props.isMobileDevice() ? 1 : 2}>
    //                 {this.state.campuses.map((campus) => {
    //                     return (
    //                         <Grid.Column className='campus-div' height={400} style={{height: `${this.props.isMobileDevice() ? 'calc(100vw / 3)' : 'calc(50vw / 3)'}`,
    //                             backgroundImage: `url(${campus.img})`,
    //                             backgroundPosition: 'bottom',
    //                             backgroundSize: 'cover' }}
    //                             onClick={() => {
    //                                 this.props.changeCampus(campus);
    //                         }}></Grid.Column>
    //                     );
    //                 })}
    //             </Grid>
    //         </div>
    //     );
    // }

    // renderDeprecated(){
    //     return (
    //         <div className={!this.props.isMobileDevice() ? 'section' : 'section-mobile'} id={this.props.isMobileDevice() ? 'map-div-mobile' : ''} style={{backgroundColor: '#E8E8E8'}}>
    //             <div className='si-group'>
    //                 <h1 className={this.props.isMobileDevice() ? 'top-left-title-mobile' : 'top-left-title'}>Choose a UH campus
    //                  {this.props.isMobileDevice() ? '' : <span>
    //                         <select id='campus-select' onChange={this.changeCampusImage.bind(this)}>
    //                             {this.state.campuses.map((campus, index) => {
    //                                 return (<option value={index}>{campus.name}</option>);
    //                             })}
    //                         </select>
    //                     </span>}
    //                 </h1>
    //                 {!this.props.isMobileDevice() ? '' :
    //                         <select id='campus-select-mobile' onChange={this.changeCampusImage.bind(this)}>
    //                             {this.state.campuses.map((campus, index) => {
    //                                 return (<option value={index}>{campus.name}</option>);
    //                             })}
    //                         </select>}
    //                 <br/>

                    
    //                     {this.props.isMobileDevice() ? (
    //                         <div id='map-div' style={{width: '94vw', textAlign: 'center'}}>
    //                         <img id='map-img' src={this.state.selectedCampusImg} style={{position: 'relative', height: 'auto', width: '94vw'}}/>
    //                         </div>
    //                     ) : (

    //                         <div id='map-div' style={{width: '90vw', textAlign: 'center'}}>
    //                         <img usemap='#image-map' id='map-img' src={this.state.selectedCampusImg} style={{position: 'relative', height: '50vh', width: 'auto'}}/>
    //                         <map name="image-map">
    //                             <area shape="poly" coords="22,22,231,22,264,82,232,143,22,143"/>
    //                             <area shape="poly" coords="233,22,443,22,476,82,442,144,233,143,264,82"/>
    //                             <area shape="poly" coords="445,22,654,22,686,81,654,143,444,143,475,82"/>
    //                             <area shape="poly" coords="655,22,895,22,895,142,655,142,684,82"/>
    //                         </map>
    //                         </div>
    //                     )}
                        



    //             </div>
                    
    //             <div style={{width: '100vw', position: 'absolute', textAlign: 'center', marginTop: 'calc(100vh - 100px)'}}>
    //                 <div class={this.props.isMobileDevice() ? "new-next-button-mobile" : 'new-next-button'} onClick={() => this.props.getSectorsForCampus()}>Next</div>
    //             </div>
    //         </div>
    //     );
    // }
}

export default MapPicker;

