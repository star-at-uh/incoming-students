import React from 'react';
import { Grid, Container, List } from 'semantic-ui-react';
import { isNull } from 'util';

class ChooseCareer extends React.Component {
    constructor(props){
        super(props);
        this.chooseSector.bind(this);
        this.state = {
            selectedSectorId: null,
            sectors: []
        };
    }

    componentDidMount(){
        this.props.scrollToSection(5);
    }

    chooseSector(id){
        let sector;

        // reset if selected
        if(!isNull(this.state.selectedSectorId)){
            sector = document.getElementById(`sectors-${this.state.selectedSectorId}`);
            sector.classList.remove(!this.props.isMobileDevice() ? 'selected-sector' : 'selected-sector-mobile');
        }

        // set next
        sector = document.getElementById(`sectors-${id}`);
        sector.classList.add(!this.props.isMobileDevice() ? 'selected-sector' : 'selected-sector-mobile');
        this.setState({selectedSectorId: id});

        this.props.chooseSector(this.props.sectors[id]);
    }

    reset

    sectorBlock(name, img, id){
        return (
            <table id={`sectors-${id}`} class='sector-block'>
                <tr>
                    <td>
                        <img class='sector-block-image' src={img}/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2 class='sector-block-name'>{name}</h2>
                    </td>
                </tr>
            </table>
        );
    }

    sectorBlockMobile(name, img, id){
        return (
            <div id={`sectors-${id}`} class='sector-block-mobile'>
                <center>
                <img class='sector-block-image-mobile' src={img}/>
                <h2 class='sector-block-name-mobile'>{name}</h2>
                </center>
            </div>
        );
    }

    mobileChooseCareerHeight(){
        let windowHeight = window.innerHeight;
        let suggestedHeight = 200 + (170 * Math.ceil(this.props.sectors.length / 2));
        return windowHeight > suggestedHeight ? windowHeight : suggestedHeight ;
    }

    render(){
        return (
            <div className={!this.props.isMobileDevice() ? 'choose-career-section' : 'section-mobile'} style={{backgroundColor:'white',
                height: this.props.isMobileDevice() ? this.mobileChooseCareerHeight()
                        : this.props.chooseCareerSectorHeight()}}>
                <Container className='career-sector-grid'>
                    {!this.props.isMobileDevice() ?
                        <div className='career-sector-title-group'>
                            <h1></h1><h1></h1>
                            <h4 style={{margin: 0}}>Campus: {this.props.campus.name}</h4>
                            <h1 style={{color: 'green', margin: 0}}>Choose a career sector</h1>
                        </div> : 
                        <div className='career-sector-title-group-mobile'>
                            <h1></h1>
                            <h2 style={{margin: 0}}>Campus: {this.props.campus.name}</h2>
                            <h1 style={{color: 'green', margin: 0}}>Choose a career sector</h1>
                            <h1></h1>
                        </div>
                    }
                    {!this.props.isMobileDevice() ? <Grid>
                        <Grid.Row columns={
                            '4'
                        } centered className="sector-grid">
                            {this.props.sectors.map((sector, index) => {
                                return (
                                    <Grid.Column className='sector-block-column'
                                        onClick={() => this.chooseSector(index)}>
                                        {this.sectorBlock(sector.ReturnAdultSectorDesc, sector.ReturnAdultSectorImageUrl, index)}
                                    </Grid.Column>
                                )
                            })}
                        </Grid.Row>
                    </Grid> : 
                     <Grid>
                        <Grid.Row columns={
                            '2'
                        } centered className="sector-grid-mobile">
                            {this.props.sectors.map((sector, index) => {
                                return (
                                    <Grid.Column className='sector-block-column-mobile'
                                        onClick={() => this.chooseSector(index)}>
                                        {this.sectorBlockMobile(sector.ReturnAdultSectorDesc, sector.ReturnAdultSectorImageUrl, index)}
                                    </Grid.Column>
                                )
                            })}
                        </Grid.Row>
                    </Grid>

                    // save incase gary changes mind, will be too hard to revert later... if u know what i mean lmao
                    // <List divided relaxed>
                    //     {this.props.sectors.map((sector, index) => {
                    //             return (
                    //                 <List.Item onClick={() => this.chooseSector(index)} style={{textAlign: 'center'}}  id={`sectors-${index}`}>
                    //                     <List.Content>
                    //                         <List.Icon as='img' src={sector.ReturnAdultSectorImageUrl} verticalAlign='middle' height={'25px'} style={{display: 'inline'}}/>
                    //                         <List.Header as='a'>
                    //                             {sector.ReturnAdultSectorDesc}
                    //                         </List.Header>
                    //                     </List.Content>
                    //                 </List.Item>
                    //             )
                    //     })}
                    // </List>
                }
                </Container>
            </div>
        )
    }
}

export default ChooseCareer;