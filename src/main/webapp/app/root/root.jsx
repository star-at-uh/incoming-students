import React from 'react';
import 'semantic-ui-css/semantic.css';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import Main from '../controllers/main/main.jsx';
import axios from 'axios';



/** Top-level layout component for this application. Called in imports/startup/client/startup.jsx. */
class App extends React.Component {
  render() {
    return (
        <Router>
          <Switch>
            <Route exact path="/" component={Main}/>
          </Switch>
        </Router>
    );
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

if(window.location.href.indexOf('localhost') == -1){
  console.warn = () => {};
  console.error = () => {};
}