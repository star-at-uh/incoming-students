import React from 'react';
import { Container, Progress, Icon } from 'semantic-ui-react';

class CareerInfo extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            
        }
    }

    componentDidMount(){
        this.props.scrollToSection(6);
    }

    render(){
        return (
            <div className={!this.props.isMobileDevice() ? 'section' : 'section-mobile'} id={this.props.isMobileDevice() ? 'career-info-div-mobile' : ''} style={{backgroundColor: '#E8E8E8'}}>
                <div className='si-group no-space'>
                    <div className={!this.props.isMobileDevice() ? 'career-sector-title-group' : 'career-info-title-group-mobile'}>
                        <h2 style={{margin: 0, verticalAlign: 'middle'}}>
                        <img className='chosen-sector-image' src={this.props.career.ReturnAdultSectorImageUrl}/>
                        <span style={{color: 'green', margin: 0}}>Career sector: </span>{this.props.career.ReturnAdultSectorDesc}</h2>
                    </div>
                    <br/><br/>
                    <br/><br/>
                    <br/><br/>
                </div>
            </div>
        );
    }
}

export default CareerInfo;

