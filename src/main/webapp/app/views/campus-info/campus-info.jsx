import React from 'react';
import { Grid, GridColumn } from 'semantic-ui-react';
import { campusInfo } from './campus-info-text';

class CampusInfo extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            
        }
    }

    componentDidMount(){
        this.props.scrollToSection(4);
    }

    render(){
        const campus = campusInfo.filter((campus) => campus.id ==[this.props.campusId])[0];
        return (
            <div className={!this.props.isMobileDevice() ? 'section centered' : 'section-mobile centered'} style={{backgroundColor: '#E8E8E8',
                height: this.props.isMobileDevice() ? '900px' : '100vh'}}>
                <div className='ci-title-group'>
                    <br/>
                    <br/>
                    <br/>
                    <h1 style={{margin: 0}}>You've Selected <span style={{color: 'green'}}>{campus.name}</span></h1>
                </div>
                <p className={!this.props.isMobileDevice() ? 'campus-text' :'campus-text-mobile'}>{campus.text}</p>
                {!this.props.isMobileDevice() ? <Grid centered>
                        <div className='ci-plain-button' onClick={() => this.props.scrollToSection(3)}>Select a Different Campus</div>
                        <div className='ci-green-button' onClick={() => this.props.scrollToSection(5)}>View Careers</div>
                </Grid> : <div>
                        <div className='ci-plain-button-mobile' onClick={() => this.props.scrollToSection(3)}>Select a Different Campus</div>
                        <div className='ci-green-button-mobile' onClick={() => this.props.scrollToSection(5)}>View Careers</div>
                    </div>}
            </div>
        );
    }
}

export default CampusInfo;

