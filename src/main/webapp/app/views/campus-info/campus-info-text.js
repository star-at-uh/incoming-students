const campusInfo = [
    {
        code: 'MAN',
        name: 'UH Manoa',
        id: 1,
        text: 'Established in 1907, the University of Hawai\'i at Manoa  is a diverse campus offering over 100 student groups and clubs for all backgrounds. It is the largest and oldest of the 10 UH campuses. Manoa offers hundreds of undergraduate, graduate and professional degrees; a strong, vital research program; and nationally ranked NCAA Division I athletics.'
    },
    {
        code: 'HIL',
        name: 'UH Hilo',
        id: 4,
        text: 'The Big Island of Hawai\'i is a natural living laboratory of active volcanoes, deep oceans, the world\'s best telescopes and a rich cultural landscape. UH Hilo bachelor\'s and master\'s degree programs take advantage of it all.'
    },
    {
        code: 'HAW',
        name: 'Hawai\'i CC',
        id: 5,
        text: 'Based in Hilo on Hawai\'i Island, Hawai\'i Community College offers associate degree, certificate and non-credit programs ranging from health services and hotel operations to business and trades. Hawai\'i Community College-Palamanui is a state-of-the-art branch campus in Kona and hosts the University Center, West Hawai\'i.'
    },
    {
        code: 'KAU',
        name: 'Kaua\'i CC',
        id: 9,
        text: 'A primary resource center and gathering place in Lihu\'e for residents and visitors, Kaua\'i Community College offers business, technology, hospitality, health, early childhood education and liberal arts courses and administers the UH Center on Kaua\'i.'
    },
    {
        code: 'KAP',
        name: 'Kapi\'olani CC',
        id: 2,
        text: 'On the slopes of Diamond Head, just minutes from Waikiki, Kapi\'olani Community College is home to the Culinary Institute of the Pacific and programs in business, hospitality, health, legal education and arts and sciences.'
    },
    {
        code: 'HON',
        name: 'Honolulu CC',
        id: 3,
        text: 'Located near downtown Honolulu with additional facilities for aeronautic, marine, automotive and heavy equipment programs, Honolulu Community College offers a strong liberal arts curriculum in addition to a wide range of career and technical degree and certificate programs as well as non-credit options.'
    },
    {
        code: 'MAU',
        name: 'Maui College',
        id: 8,
        text: 'Maui College serves three islands, providing comprehensive opportunities and special programs on Maui, with additional education centers on Lana\'i and Moloka\'i. A pioneering television network helps reach rural areas.'
    },
    {
        code: 'WOA',
        name: 'UH West O\'ahu',
        id: 10,
        text: 'A four-year, indigenous-serving institution, UH West O\'ahu provides an intellectually challenging higher education experience in a supportive setting, offering more than 30 academic concentrations in six degree programs designed for career success.'
    },
    {
        code: 'WIN',
        name: 'Windward CC',
        id: 7,
        text: 'The lush serenity at the base of O\'ahu\'s Ko\'olau mountains in Kane\'ohe provides a backdrop for Windward Community College\'s supportive education in visual and performing arts, veterinary technology, natural and environmental sciences, Hawaiian studies and exploratory programs in marine and aerospace fields.'
    },
    {
        code: 'LEE',
        name: 'Leeward CC',
        id: 6,
        text: 'Overlooking historic Pearl Harbor, Leeward Community College offers comprehensive opportunities from career training to four-year transfer pathways. Outstanding programs include Hawaiian studies, teacher education, IT security, culinary arts, engineering and the sciences. Free tutoring and job placement assistance are focused on helping students reach their educational goals.'
    } 
];

export { campusInfo };