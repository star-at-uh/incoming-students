import React from 'react';
import { Grid, Container, Progress, Icon } from 'semantic-ui-react';
import ReactSpeedometer from "react-d3-speedometer"

class StudentInfo extends React.Component {
    constructor(props){
        super(props);
    }

    componentDidMount(){
        this.props.scrollToSection(2);
    }

    getCampusName(campusCode){
        let campusKey = ''
        switch(parseInt(campusCode)){
            case 1:
                campusKey = 'UH Manoa'
                break;
            case 2:
                campusKey = 'Kapi\'olani CC'
                break;
            case 3:
                campusKey = 'Honolulu CC'
                break;
            case 4:
                campusKey = 'UH Hilo'
                break;
            case 5:
                campusKey = 'Hawai\'i CC'
                break;
            case 6:
                campusKey = 'Leeward CC'
                break;
            case 7:
                campusKey = 'Windward CC'
                break;
            case 8:
                campusKey = 'Maui College'
                break;             
            case 9:
                campusKey = 'Kaua\'i CC'
                break;                      
            case 10:
                campusKey = 'UH West O\'ahu'
                break;
            default:
                campusKey = 'N/A'
        }
        return campusKey;
    }

    render(){
        return (
            <div className={!this.props.isMobileDevice() ? 'section' : 'section-mobile'} id={this.props.isMobileDevice() ? 'student-info-div-mobile' : ''} style={{backgroundColor: '#E8E8E8'}}>
            {!this.props.isMobileDevice() ? ( <div>
                <div className='si-group'>
                    <div className='si-title-group'>
                    <br/>
                    <h1 style={{margin: 0}}>Check it out!</h1>
                    </div>
                    <br/>
                    <div id="speedo">
                    <ReactSpeedometer
                        width='500'
                        maxValue={100}
                        value={this.props.student.percentComplete}
                        needleColor="gray"
                        startColor="#C0EAC6"
                        maxSegmentLabels={10}
                        segments={1000}
                        endColor="#1DBC59"
                        ringWidth={180}
                        needleTransitionDuration={1000}
                    />
                    </div>
                    <h2 style={{ color: '#2f2f2f' }}>You are {this.props.student.percentComplete}% finished with your first semester!</h2>
                    <br/>
                    <h2 style={{ color: '#2f2f2f' }}>See how your credits apply towards a UH Campus</h2>
                    
                    <br/>
                    <div className="new-speedo-button" onClick={() => this.props.scrollToSection(3)}>Choose A Campus</div>
                    
                </div>
            </div>) : (

<div>
<div className='si-group'>
    <div className='si-title-group'>
    <br/>
    <h1 style={{margin: 0}}>Check it out!</h1>
    </div>
    <br/>
    <div id="speedo-mobile">
    <ReactSpeedometer
        width='300'
        maxValue={100}
        value={this.props.student.percentComplete}
        needleColor="gray"
        startColor="#C0EAC6"
        maxSegmentLabels={10}
        segments={1000}
        endColor="#1DBC59"
        ringWidth={80}
        needleTransitionDuration={1000}
    />
    </div>
    <h2 style={{ color: '#2f2f2f' }}>You are {this.props.student.percentComplete}% finished with your first semester!</h2>
    <br/>
    <h2 style={{ color: '#2f2f2f' }}>See how your credits apply towards a UH Campus</h2>
    
    <br/>
    <div className="new-speedo-button" onClick={() => this.props.scrollToSection(3)}>Choose A Campus</div>
    
</div>
</div>)}
            </div>
        );
    }
}

export default StudentInfo;

