import React from 'react';
import { Container } from 'semantic-ui-react';

export default class Landing extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <div className={this.props.isMobileDevice() ? 'main-landing-mobile section' : 'main-landing-image section'} id='landing-div'>
        <img id={this.props.isMobileDevice() ? 'landing-logo-mobile' : 'landing-logo'} src='public/images/UH_Seal.svg'></img>
        <div id={!this.props.isMobileDevice() ? 'welcome-back' : 'welcome-back-mobile'}>
          <h1 id={this.props.isMobileDevice() ?'landing-header-mobile':'landing-header'}>WELCOME!</h1>
          <p id={this.props.isMobileDevice() ? 'landing-desc-mobile':'landing-desc'}>Let us help you with<br/>your college journey.</p>
          {this.props.isMobileDevice() ? <div id='new-enter-button-mobile' onClick={() => this.props.scrollToSection(1)}>ENTER</div> : 
          <div id='new-enter-button' onClick={() => this.props.scrollToSection(1)}>ENTER</div>}
        </div>
      </div>
    )
  }
}