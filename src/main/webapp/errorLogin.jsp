<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"    %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"     %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"     %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="java.util.*,edu.hawaii.star.model.user.User" %>
<%
  response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
  response.setHeader("Pragma","no-cache"); //HTTP 1.0
  response.setHeader("X-UA-Compatible","IE=7"); //IE9 compatiblity
  response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>

<%--
<jsp:forward page="DevelopmentVariables.jsp"/>
--%>

<c:set var="debug"  value="false"/>
<c:set var="AdviserFirstName" value="unknown"/>
<c:set var="AdviserLastName" value="unknown"/>
<c:set var="UserID" value="unknown"/>
<c:set var="affiliation" value="unknown"/>
<c:set var="IsStaff" value="false"/>
<c:set var="IsStudent" value="false"/>
<c:set var="IsAuthenticated" value="false"/>
<c:set var="IsAuthorized" value="false" />
<c:set var="IsAuthorizedBanner" value="false"/>

<c:if test="${param.uid == 'breti'}">
  <c:set var="debug"  value="true"/>
</c:if>
<c:if test="${debug == 'true'}">
  <%
    System.out.println("Error Login page");
  %>
</c:if>

<c:if test="${ not empty requestScope.user }">
  <%
    User user = (User) request.getAttribute("user");

    pageContext.setAttribute("UserID", user.getId());
    pageContext.setAttribute("AdviserLastName", user.getSurnameName());
    pageContext.setAttribute("AdviserFirstName", user.getGivenName());
    pageContext.setAttribute("affiliation", user.getAffiliation());

    // checks if the user is a student or other.
    boolean isStaff = User.isStaffAffiliated(user);
    boolean isStudent = false;
    if (!isStaff) {
      isStudent = (User.isStudentAffiliated(user) || User.isOtherAffiliated(user));
    }

    pageContext.setAttribute("IsStaff", isStaff);
    pageContext.setAttribute("IsStudent", isStudent);

    boolean isAuthenticated = user.isAuthenticated();
    pageContext.setAttribute("IsAuthenticated", isAuthenticated);
  %>
  <c:if test="${debug == 'true'}">
    <%
      System.out.println("Printing user");
      System.out.println(user);
    %>
  </c:if>
</c:if>

<%--
  Run only if previousPage was not processorBannerCheck, UserID has been set, and there was a successful connection to the LDAP server.
--%>
<%-- <c:if test="${ requestScope.previousPage != 'processorBannerCheck'
  && empty lockOut}">

  <c:if test="${debug == 'true'}">
    <%
      System.out.println("EXEC: SecurityLog_Main_runProc");
    %>
  </c:if>

  <sql:query var="results">
    exec SecurityLog_Main_runProc ?,?,?,?,?
    <sql:param value="${ UserID }" />
    <sql:param value="${ pageContext.request.remoteAddr }" />
    <sql:param value="N" />
    <sql:param value="${ initParam.version }" />
    <sql:param value="error" />
  </sql:query>
  <c:set var="lockOut" value="${ results.rows[0].Decision }"/>
</c:if> --%>

<%--
  Run only if UserID has been set, there was a successful connection to the LDAP server,
  we have not been locked out, the website is not closed
--%>
<%-- <c:if test="${ lockOut != 'LockOut' }">

  <c:if test="${debug == 'true'}">
    <%
      System.out.println("EXEC: SecurityLogonAudit_subProc");
    %>
  </c:if>
  <sql:query var="LogonAudit">
    exec AdvisingBannerDB.SecurityLogonAudit_subProc ?, ?, ?, ?, ?, ?, ?, ?, ?
    <sql:param value="${ pageScope.AdviserFirstName }" />
    <sql:param value="${ pageScope.AdviserLastName }" />
    <sql:param value="${ pageScope.UserID }" />
    <sql:param value="${ pageScope.affiliation }" />
    <sql:param value="${ pageContext.request.remoteAddr }" />
    <sql:param value="${ header['user-agent'] }" />
    <sql:param value="${ initParam.version }" />
    <sql:param value="N" />
    <sql:param value="error" />
  </sql:query>
</c:if> --%>

<%-- <c:if test="${ initParam.applicationType == 'advising'
  && not IsStaff }">
  <c:if test="${debug == 'true'}">
    <%
      System.out.println("EXEC: SecurityIfStudentCheckAuthorized_subProc");
    %>
  </c:if>
  <sql:query var="authorized">
    SecurityIfStudentCheckAuthorized_subProc ?
    <sql:param value="${ pageScope.UserID }"/>
  </sql:query>

  <c:set var="IsAuthorized" value="${ authorized.rows[0].StudentAuthorized }" />
  <c:set var="authorizationErrorMessage" value="${ authorized.rows[0].MessageID }" />
</c:if> --%>
<%-- 
<c:if test="${ IsAuthenticated
  && (IsStaff
  || (IsStudent
  && IsAuthorized)) }">
  <c:if test="${debug == 'true'}">
    <%
      System.out.println("EXEC: Security_CheckAccessToBannerFormsProc");
    %>
  </c:if>
  <sql:query var="security">
    exec Security_CheckAccessToBannerFormsProc ?
    <sql:param value="${ sessionScope.UserID }"/>
  </sql:query>
  <c:set var="IsAuthorizedBanner" value="${ security.rows[ 0 ].Athenticated }"/>
</c:if> --%>

<%--
  There is a specific order to these conditions:
  UserId not set
  Invalid HTTP Method (GET)
  LDAP connection failed
  Too many login attempts
  Website is closed
  Failed login
  LDAP Staff affiliation failed
  Authorized Student Status
  Banner Access
  Unknown Error
--%>
<c:choose>

  <c:when test="${ requestScope.LDAPStatus == 'InvalidHTTPMethod' }">
    <c:set var="messageID" value="http-method-invalid"/>
  </c:when>

  <c:when test="${ empty pageScope.UserID
    || pageScope.UserID == '' }">
    <c:set var="messageID" value="ldap-user-not-found"/>
  </c:when>

  <c:when test="${ requestScope.LDAPStatus == 'FailedConnection' }">
    <c:set var="messageID" value="ldap-connection"/>
  </c:when>

  <c:when test="${ lockOut == 'LockOut' }">
    <c:set var="messageID" value="lock-out"/>
  </c:when>


  <c:when test="${ webClosed == 'true' }">
    <c:set var="messageID" value="closed-for-maintenance"/>
  </c:when>


  <c:when test="${ requestScope.LDAPStatus == 'UserNotAuthorized'
    || (initParam.applicationType == 'advising'
    && IsAuthenticated
    && not IsStaff
    && not IsStudent) }">
    <c:set var="messageID" value="incorrect-affiliation"/>
  </c:when>


  <c:when test="${requestScope.LDAPStatus == 'StudentNotAuthorized'
    || requestScope.LDAPStatus == 'StudentUnauthorized'
    || (initParam.applicationType == 'advising'
    && IsStudent
    && IsAuthenticated
    && not IsAuthorized) }">
    <c:set var="messageID" value="${authorizationErrorMessage}"/>
  </c:when>


  <c:when test="${ requestScope.LDAPStatus == 'UserNotFound'
    || requestScope.LDAPStatus == 'PasswordIncorrect'
    || not IsAuthenticated }">
    <c:set var="messageID" value="password-invalid"/>
  </c:when>


  <c:when test="${ initParam.applicationType == 'advising' && not IsAuthorizedBanner }">
    <c:set var="messageID" value="banner-access-code-invalid"/>
  </c:when>

  <c:when test="${ not empty requestScope.eMessage }">
    <c:set var="msgTitle" value="Error"/>
    <c:set var="msgBd">
      ${ requestScope.eMessage }
    </c:set>
  </c:when>


  <c:otherwise>
    <c:set var="msgTitle" value="Unknown"/>
    <c:set var="msgBd">
      Unknown Error!
    </c:set>
        <%
            StringBuffer sb = new StringBuffer( "Session Scope :<br/>" );

            Enumeration<String> values = session.getAttributeNames();

            while (values.hasMoreElements()){
              String key=values.nextElement();
              sb.append( key ).append( "=>").append( session.getAttribute( key )) .append( "<br/>" );
            }

            sb.append("<br/>Request Scope:<br/>" );

            Enumeration<String> e = request.getAttributeNames();
            while( e.hasMoreElements() ) {
                String key = (String) e.nextElement();
                sb.append( key ).append( "=>").append( request.getAttribute( key ) ).append( "<br/>" );
            }

            pageContext.setAttribute( "variables", sb.toString() );
        %>
  </c:otherwise>
</c:choose>

<sql:query var="messageResults">
  DegreeAuditDB.dbo.ViewDegreeAuditInterfaceMessage ?
  <sql:param value="${messageID}"/>
</sql:query>
<c:set var="msgTitle" value="${messageResults.rows[0].MessageTitle}"/>
<c:set var="msgBd" value="${messageResults.rows[0].MessageText}"/>

<c:set var="msgTitle" scope="page">
  Login Error: ${ pageScope.msgTitle }
</c:set>
<c:set var="msgHd" scope="page">
  - Login Error -
</c:set>
<c:set var="msgBd" scope="page">
  <div class="mBottomB">
    <c:out value="${ pageScope.msgBd }" escapeXml="false"/>
  </div>
  <a href="./index.jsp">Return to Login Page</a>
</c:set>
<c:set var="msgFt">
  REMINDER: Your usage and computer IP address is being monitored.
</c:set>
<c:set var="pageType" value="Error"/>
<%@ include file="GeneralMessage.jsp" %>
<% session.invalidate(); %>