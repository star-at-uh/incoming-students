<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"		 %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"		 %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"		 %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${ not empty sessionScope.msgHd }">
	<c:set var="msgHd"	value="${ sessionScope.msgHd }" scope="page"/>
	<c:remove var="msgHd" scope="session"/>
</c:if>
<c:if test="${ not empty sessionScope.msgBd }">
	<c:set var="msgBd"	value="${ sessionScope.msgBd }" scope="page"/>
	<c:remove var="msgBd" scope="session"/>
</c:if>
<c:if test="${ not empty sessionScope.msgFt }">
	<c:set var="msgFt"	value="${ sessionScope.msgFt }" scope="page"/>
	<c:remove var="msgFt" scope="session"/>
</c:if>
<c:if test="${ not empty sessionScope.msgHeader }">
	<c:set var="msgHeader"	value="${ sessionScope.msgHeader }" scope="page"/>
	<c:remove var="msgHeader" scope="session"/>
</c:if>
<c:if test="${ not empty sessionScope.pageType }">
	<c:set var="pageType"	value="${ sessionScope.pageType }" scope="page"/>
	<c:remove var="pageType" scope="session"/>
</c:if>

<c:choose>
  <c:when test="${ onlyMessage == 'true' }">
    <div class="errorMessage">
      <div class="hd">
        ${ fn:trim( pageScope.msgHd ) }
      </div>
      <div class="bd">
        ${ fn:trim( pageScope.msgBd ) }
      </div>
      <div class="ft">
        ${ fn:trim( pageScope.msgFt ) }
      </div>
    </div>
  </c:when>
  <c:otherwise>
    <html>
    <head>
    <meta http-equiv="X-UA-Compatible" content="IE=7"/>
      <title><c:out value="${ fn:trim( pageScope.msgHeader ) }" escapeXml="false"/></title>
    
      <script type="text/javascript" src="/includes/yui/2.9.0/build/utilities/utilities.js"    ></script>
      <script type="text/javascript" src="/includes/yui/2.9.0/build/container/container-min.js"  ></script>
      <script type="text/javascript" src="/includes/yui/2.9.0/build/button/button-min.js"      ></script>
      <link rel="stylesheet" type="text/css" href="/includes/yui/2.9.0/build/reset-fonts-grids/reset-fonts-grids.css"  />
      <link rel="stylesheet" type="text/css" href="/includes/yui/2.9.0/build/container/assets/skins/sam/container.css" />
      <link rel="stylesheet" type="text/css" href="/includes/yui/2.9.0/build/button/assets/skins/sam/button.css"   />
    
      <link rel="stylesheet" type="text/css" href="/includes/style/Background.css" />
      <link rel="stylesheet" type="text/css" href="/includes/style/Borders.css"    />
      <link rel="stylesheet" type="text/css" href="/includes/style/Cursor.css"   />
      <link rel="stylesheet" type="text/css" href="/includes/style/DialogBoxes.css"  />
      <link rel="stylesheet" type="text/css" href="/includes/style/Dimensions.css" />
      <link rel="stylesheet" type="text/css" href="/includes/style/FontText.css"   />
      <link rel="stylesheet" type="text/css" href="/includes/style/List.css"     />
      <link rel="stylesheet" type="text/css" href="/includes/style/MarginPadding.css"/>
      <link rel="stylesheet" type="text/css" href="/includes/style/Misc.css"     />
    
      <link rel="stylesheet" type="text/css" href="/includes/style/GeneralMessage.css"/>
    </head>
    <body id="${ pageScope.pageType }" class="pTopB">
      <div id="doc-custom">
        <div id="hd" class="dimH2 tCenter pTopS">
          <c:out value="${ fn:trim( pageScope.msgHd ) }" escapeXml="false"/>
        </div>
        <div id="bd" class="pAllB">
          <c:out value="${ fn:trim( pageScope.msgBd ) }" escapeXml="false"/>
        </div>
        <div id="ft" class="tCenter pAllB">
          <c:out value="${ fn:trim( pageScope.msgFt ) }" escapeXml="false"/>
        </div>
      </div>
    </body>
    </html>  
  </c:otherwise>
</c:choose>